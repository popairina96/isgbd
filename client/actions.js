$(document).ready(function () {
	// test();
	var columns = [];
	var pk = [];
	var length_columns_array = 0;
	var checkedIsNull = false;
	var checkedIsPK = false;
	var checkedIsFK = false;

	$("#btnDBs").click(function () {
		if ($('.showDBs').length == 0) {
			$.ajax({
				url: "http://localhost:8080/dbs/getDbs",
				type: "get",
				success: function (response) {
					// console.log(response.length);   
					let dbs = "<ul class='showDBs show'>";
					for (var i = 0; i < response.length; i++) {
						dbs += "<li id='btn" + response[i] + "'><input type='button' class='btn btn-secondary' value='" + response[i] + "' onclick='showTables(&quot;" + response[i] + "&quot;)'>" +
							" <button type='button' class='btn btn-link' style='color:red' onclick='deleteDatabase(&quot;" + response[i] + "&quot;)'>Delete</button>  "
							+
							"<button id='btnClick' type='button' class='btn btn-link' onclick='showFormAddTable(&quot;" + response[i] + "&quot;)'>Add table</button></li>";
					}
					dbs += "</ul>";
					$("#liDBs").append(dbs);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				}
			});
		}
		else {
			if ($(".showDBs").hasClass("show")) {
				$(".showDBs").removeClass("show");
				$(".showDBs").addClass("hide");
			} else {
				$(".showDBs").removeClass("hide");
				$(".showDBs").addClass("show");
			}
		}
	});

	$("#btnShowFormAddDb").click(function () {
		$(".form.show").removeClass("show");
		$(".form").addClass("hide");
		$("#formAddDb").removeClass("hide");
		$("#formAddDb").addClass("show");

	});

	$("#btnCreateDB").click(function () {
		var input = $("#inputDB").val();
		console.log(input);
		if (input) {
			if (input.indexOf(" ") < 0) {
				$.ajax({
					url: "http://localhost:8080/dbs",
					type: "post",
					data: { "name": input },
					success: function (response) {

						if ($('.showDBs').length != 0) {
							$(".showDBs").append("<li id='btn" + input + "'><input type='button' class='btn btn-secondary' value='" + input + "' onclick='showTables(&quot;" + input + "&quot;)'>" +
								" <button type='button' class='btn btn-link' style='color:red' onclick='deleteDatabase(&quot;" + input + "&quot;)'>Delete</button>"
								+
								"<button id='btnClick' type='button' class='btn btn-link' onclick='showFormAddTable(&quot;" + input + "&quot;)'>Add table</button></li>");
						}
						console.log(response);
						$("#successMessage").html("Success");
						$("#successMessage").removeClass("hide");
						$("#formAddDb").removeClass("show");
						$("#formAddDb").addClass("hide");
						$("#inputDB").val("");
						setTimeout(function () {
							$("#successMessage").addClass("hide");
						}, 2000);
					},
					error: function (jqXHR, textStatus, errorThrown) {
						// error: function(response) {
						// console.log
						$("#successMessage").html("This database already exist!");
						$("#successMessage").removeClass("hide");
						$("#successMessage").removeClass("btn-success");
						$("#successMessage").addClass("btn-danger");
						setTimeout(function () {
							$("#successMessage").addClass("hide");
							$("#successMessage").removeClass("btn-danger");
							$("#successMessage").addClass("btn-success");
						}, 2000);
						// console.log(textStatus, errorThrown);
					}
				});
			} else {
				$("#successMessage").html("Database name should not contain spaces!");
				$("#successMessage").removeClass("hide");
				$("#successMessage").removeClass("btn-success");
				$("#successMessage").addClass("btn-danger");
				setTimeout(function () {
					$("#successMessage").addClass("hide");
					$("#successMessage").removeClass("btn-danger");
					$("#successMessage").addClass("btn-success");
				}, 2000);
			}
		} else {
			$("#successMessage").html("Database is empty!");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function () {
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);
		}
	});

	$("#btnAddTable").click(function () {
		var databaseName = $("#insertInDb").html();

		var tableName = $("#inputTableName").val();
		var rowLength = $("#inputTableRowLength").val();

		if (tableName && rowLength && columns.length > 0) {
			$.ajax({
				url: "http://127.0.0.1:8080/dbs/addTable",
				type: "POST",
				data: {
					"database": databaseName,
					"table": tableName,
					"rowLength": rowLength,
					"fields": columns.toString(),
					"foreignKeys": pk.toString()
				},
				success: function (response) {
					$("#inputTableName").val("");
					$("#inputTableRowLength").val("");
					$("#fieldsAdded").html("");
					emptyColumns();
					columns = [];
					pk = []
					//adauga tabel la lista de tabele
					let className = ".tables" + databaseName;
					if ($(className).length != 0) {
						let table = "<li id='btn" + tableName + databaseName + "' ><input class='btn btn-info' type='button' value='" + tableName + "' onclick='showColumns(&quot;" + tableName + "&quot;)'>" +
							"<button type='button' class='btn btn-link' style='color:red' onclick='deleteTable(&quot;" + tableName + "&quot;,&quot;" + databaseName + "&quot;)'>Delete</button> "
							+
							"<button type='button' class='btn btn-link' style='color:green' onclick='showIndexForm(&quot;" + tableName + "&quot;,&quot;" + databaseName + "&quot;)'>Add index</button> "
							+
							"<button type='button' class='btn btn-link' style='color:black' onclick='showInsertRecordForm(&quot;" + tableName + "&quot;,&quot;" + databaseName + "&quot;)'>Insert record</button> "
							+
							"<button type='button' class='btn btn-link' style='color:purple' onclick='showDeleteRecordForm(&quot;" + tableName + "&quot;,&quot;" + databaseName + "&quot;)'>Delete record</button> "
							+
							"<button type='button' class='btn btn-link' style='color:orange' onclick='showSelectForm(&quot;" + tableName + "&quot;,&quot;" + databaseName + "&quot;)'>Select</button> "
							+
							"<button type='button' class='btn btn-link' style='color:pink' onclick='showJoinForm(&quot;" + tableName + "&quot;,&quot;" + databaseName + "&quot;)'>Join</button> "
							+
							"<button type='button' class='btn btn-link' style='color:magenta' onclick='showOuterForm(&quot;" + tableName + "&quot;,&quot;" + databaseName + "&quot;)'>Outer Join</button> "
							+
							"<button type='button' class='btn btn-link' style='color:#ff9999' onclick='showBonusForm(&quot;" + tableName + "&quot;,&quot;" + databaseName + "&quot;)'>Semi&AntiJoin</button> "
							+
							"</li>";
						$(className).append(table);
					}
					$(".form.show").removeClass("show");
					$("#formAddTable").addClass("hide");

					$("#successMessage").html("Success");
					$("#successMessage").removeClass("hide");
					$("#formAddDb").removeClass("show");
					$("#formAddDb").addClass("hide");
					$("#inputDB").val("");
					setTimeout(function () {
						$("#successMessage").addClass("hide");
					}, 3000);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
					columns = [];
					pk = [];
					$("#successMessage").html("Database already contains this table name! ");
					$("#successMessage").removeClass("hide");
					$("#successMessage").removeClass("btn-success");
					$("#successMessage").addClass("btn-danger");
					setTimeout(function () {
						$("#successMessage").addClass("hide");
						$("#successMessage").removeClass("btn-danger");
						$("#successMessage").addClass("btn-success");
					}, 3000);
				}

			});
		} else {
			$("#successMessage").html("Fields should not be empty!");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function () {
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);

		}
	});

	$("#btnAddMoreColumns").click(function () {

		var name = $("#inputTableDetailsName").val();
		var type = $("#inputTableDetailsType").val();
		var length = $("#inputTableDetailsLength").val();

		checkedIsNull = $('#inputTableDetailsIsNull').is(':checked');
		checkedIsPK = $('#inputTableDetailsPK').is(':checked');
		checkedIsFK = $("#inputTableDetailsFK").is(':checked');
		//adauga coloanele in vector
		if (name && type && length) {

			if (checkedIsFK == true) {

				let tabelFk = $("#selectTable").val();
				let fieldFk = $("#inputField").val();
				if (tabelFk && fieldFk) {
					columns.push(name);
					columns.push(type);
					columns.push(length);

					if (checkedIsNull == false)
						columns.push(0);
					else
						columns.push(1);
					if (checkedIsPK == false)
						columns.push(0);
					else
						columns.push(1);
					pk.push(name);
					pk.push(tabelFk);
					pk.push(fieldFk);
					$("#inputField").val("");
					$("#fieldsAdded").append("Name: " + name + ", Type: " + type + ", Length: " + length + ", isNull: " + checkedIsNull + ", isPrimaryKey: " + checkedIsFK + ", isForeignKey: " + checkedIsFK);
					emptyColumns();
					$("#formFK").removeClass("show");
					$("#formFK").addClass("hide");

				} else {
					$("#successMessage").html("Fields for Fk should not be empty!");
					$("#successMessage").removeClass("hide");
					$("#successMessage").removeClass("btn-success");
					$("#successMessage").addClass("btn-danger");
					setTimeout(function () {
						$("#successMessage").addClass("hide");
						$("#successMessage").removeClass("btn-danger");
						$("#successMessage").addClass("btn-success");
					}, 3000);
				}

			} else {
				columns.push(name);
				columns.push(type);
				columns.push(length);

				if (checkedIsNull == false)
					columns.push(0);
				else
					columns.push(1);
				if (checkedIsPK == false)
					columns.push(0);
				else
					columns.push(1);
				$("#fieldsAdded").append("Name: " + name + ", Type: " + type + ", Length: " + length + ", isNull: " + checkedIsNull + ", isPrimaryKey: " + checkedIsPK + ", isForeignKey: " + checkedIsFK);
				emptyColumns();
			}
		} else {
			$("#successMessage").html("Fields should not be empty!");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function () {
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);

		}
	});
	$("#btnMultipleSelect").click(function () {
		let selectedItems = $("#selectFields").val();
		let indexName = $("#inputIndexName").val();
		let databaseName = $("#createIndexInDatabase").html();
		let tableName = $("#createIndexInTable").html();
		// for (var i =0;i< selectedItems.length; i++) {
		// 	console.log(selectedItems[i]);			
		// }
		let checkIsUnique = $('#inputUnique').is(':checked');
		console.log(checkIsUnique);

		if (selectedItems.length > 0 && indexName) {
			$.ajax({
				url: "http://127.0.0.1:8080/dbs/addIndex",
				type: "POST",
				data: {
					"database": databaseName,
					"table": tableName,
					"indexName": indexName,
					"isUnique": checkIsUnique,
					"fields": selectedItems.toString()
				},

				success: function (response) {
					$("#inputIndexName").val("");
					$(".form.show").removeClass("show");
					$("#formIndex").addClass("hide");

					$("#successMessage").html("Success");
					$("#successMessage").removeClass("hide");
					$("#formAddDb").removeClass("show");
					$("#formAddDb").addClass("hide");
					$("#inputDB").val("");
					setTimeout(function () {
						$("#successMessage").addClass("hide");
					}, 2000);
				},
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(textStatus, errorThrown);
				}

			});
		} else {
			$("#successMessage").html("Plese add a name and select at least one column! ");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function () {
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);
		}

	})

	$("#btnInsertRecord").click(function () {
		var valori = [];
		var j = 0;
		$(".lbl").each(function (i, obj) {
			console.log($(obj).text());
			valori[j] = $(obj).text();
			j = j + 2;

		})
		j = 1;
		var empty = false;
		$(".inp").each(function (i, obj) {
			console.log($(obj).val());
			valori[j] = $(obj).val();
			if (!$(obj).val()) {
				empty = true;
			}
			j = j + 2;

		});
		console.log(empty);
		var db = $("#insertRecordInDatabase").html();
		var table = $("#insertRecordInTable").html();
		console.log(db);
		console.log(table);
		console.log(valori);

		if (db && table && empty == false) {
			$.ajax({
				url: "http://127.0.0.1:8080/dbs/insertRecord",
				type: "POST",
				data: {
					"database": db,
					"table": table,
					"fields": valori.toString()
				},
				success: function (response) {
					// $("#inputTableName").val("");
					// $("#inputTableRowLength").val("");
					// $("#fieldsAdded").html("");
					// emptyColumns();
					// columns=[];
					// pk=[]

					$(".form.show").removeClass("show");
					$("#formInsertRecord").addClass("hide");
					$("#successMessage").html("Success");
					$("#successMessage").removeClass("hide");
					$(".inp").each(function (i, obj) {
						$(obj).val("");
					});

					setTimeout(function () {
						$("#successMessage").addClass("hide");
					}, 3000);
				},
				error: function (response) {
					// $("#successMessage").html("There is no record for the external key! / There is already a record with given id");
					$("#successMessage").html(response.responseText);
					$("#successMessage").removeClass("hide");
					$("#successMessage").removeClass("btn-success");
					$("#successMessage").addClass("btn-danger");
					setTimeout(function () {
						$("#successMessage").addClass("hide");
						$("#successMessage").removeClass("btn-danger");
						$("#successMessage").addClass("btn-success");
					}, 3000);
				}
			});
		} else {
			$("#successMessage").html("Fields should not be empty!");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function () {
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);

		}

	});

	$("#btnDeleteRecord").click(function () {
		var valori = [];
		var empty = false;
		$(".inpDel").each(function (i, obj) {
			valori.push($(obj).val());
			if (!$(obj).val()) {
				empty = true;
			}
		});
		var db = $("#deleteRecordInDatabase").html();
		var table = $("#deleteRecordInTable").html();
		console.log(db);
		console.log(table);
		console.log(valori);

		if (db && table && empty == false) {
			$.ajax({
				url: "http://127.0.0.1:8080/dbs/deleteRecord",
				type: "POST",
				data: {
					"database": db,
					"table": table,
					"values": valori.toString()
				},
				success: function (response) {
					$(".form.show").removeClass("show");
					$("#formDeleteRecord").addClass("hide");
					$("#successMessage").html("Success");
					$("#successMessage").removeClass("hide");
					$(".inpDel").each(function (i, obj) {
						$(obj).val("");
					});

					setTimeout(function () {
						$("#successMessage").addClass("hide");
					}, 3000);
				},
				error: function (success) {
					$("#successMessage").html(success.responseText);
					$("#successMessage").removeClass("hide");
					$("#successMessage").removeClass("btn-success");
					$("#successMessage").addClass("btn-danger");
					setTimeout(function () {
						$("#successMessage").addClass("hide");
						$("#successMessage").removeClass("btn-danger");
						$("#successMessage").addClass("btn-success");
					}, 3000);
				}
			});
		} else {
			$("#successMessage").html("Fields should not be empty!");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function () {
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);

		}

	});

	$("#btnSelect").click(function () {
		let selectedItems = $("#selectProjectionFields").val();
		let inputCondition = $("#inputCondition").val();
		let databaseName = $("#selectFromDatabase").html();
		let tableName = $("#selectFromTable").html();

		if (selectedItems.length > 0) {
			$.ajax({
				url: "http://127.0.0.1:8080/dbs/selectStatement",
				type: "POST",
				data:
					{
						"database": databaseName,
						"table": tableName,
						"condition": inputCondition,
						"fields": selectedItems.toString()
					},

				success: function (response) {
					$("#inputIndexName").val("");
					$(".form.show").removeClass("show");
					$("#formSelect").addClass("hide");
					$("#divForTable").removeClass("hide");
					$("#divForTable").addClass("show");
					$("#divForTable").html("");
					$("#divForTable").append(response);

				},
				error: function (response) {
					$("#successMessage").html(response.responseText);
					$("#successMessage").removeClass("hide");
					$("#successMessage").removeClass("btn-success");
					$("#successMessage").addClass("btn-danger");
					setTimeout(function () {
						$("#successMessage").addClass("hide");
						$("#successMessage").removeClass("btn-danger");
						$("#successMessage").addClass("btn-success");
					}, 3000);
				}

			});
		} else {
			$("#successMessage").html("Select at least one column! ");
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function () {
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);
		}

	})
	$("#selectJoinTables").change(function () {
		let tabelSelectat = $(this).val();
		if (tabelSelectat != "") {
			for (let i = 1; i < fksJoin.length; i += 3) {
				if (fksJoin[i] == tabelSelectat) {
					let v = tabelJoin + "." + fksJoin[i - 1] + "=" + tabelSelectat + "." + fksJoin[i + 1];
					console.log(v);
					$("#condEgalitate").val(v);
				}
			}
		} else {
			$("#condEgalitate").val("condition");

		}
	});

	$("#btnJoin").click(function () {
		console.log("execute");
		let db = $("#joinDb").html();
		let projectionColumns = $("#inputJoinColumns").val();
		let tabelInitial = $("#joinTable").html();
		let join = $("#selectJoinType").val();
		let tabel1 = $("#inputJoinTabel1").val();
		let condition1 = $("#inputCondition1").val();
		let tabel2 = $("#inputJoinTabel2").val();
		let condition2 = $("#inputCondition2").val();
		let groupby = $("#inputGroupBy").val();
		let having = $("#inputHaving").val();


		console.log(db);
		console.log(projectionColumns);
		console.log(tabelInitial);
		console.log(join);
		console.log(tabel1);
		console.log(condition1);
		console.log(tabel2);
		console.log(condition2);
		console.log(groupby);
		console.log(having);


		// let selectedTabel=$("#selectJoinTables").val();
		// let selectedJoin=$("#selectJoinType").val();
		// let condEgalitate=$("#condEgalitate").val();
		// let tableName=$("#joinTable").html();

		// console.log("tabel");
		// console.log(selectedTabel);

		// console.log("join");

		// console.log(selectedJoin);
		// console.log("tabel principal");
		// console.log(tableName);

		// console.log("condEgalitate");
		// console.log(condEgalitate);
		// console.log(db);
		// if(selectedTabel!=""){
		// 	console.log("intra");
		// 	$.ajax({
		// 		url: "http://127.0.0.1:8080/dbs/join", 
		// 		type: "POST",
		// 		data: {"database":db,
		// 		"table1":tableName,
		// 		"table2":selectedTabel,
		// 		"condition":condEgalitate,
		// 		"joinType":selectedJoin },

		// 		success: function (response) {
		// 			$(".form.show").removeClass("show");
		// 			$("#formJoin").addClass("hide");
		// 			$("#divForTable").removeClass("hide");
		// 			$("#divForTable").addClass("show");
		// 			$("#divForTable").html("");
		// 			$("#divForTable").append(response);
		// 		},
		// 		error: function(response) {
		// 			$("#successMessage").html(response.responseText);
		// 			$("#successMessage").removeClass("hide");
		// 			$("#successMessage").removeClass("btn-success");
		// 			$("#successMessage").addClass("btn-danger");
		// 			setTimeout(function(){ 
		// 				$("#successMessage").addClass("hide");
		// 				$("#successMessage").removeClass("btn-danger");
		// 				$("#successMessage").addClass("btn-success");
		// 			}, 3000);   
		// 		}

		// 	});
		// } else{
		// 	$("#successMessage").html("Please make the selection!  ");
		// 	$("#successMessage").removeClass("hide");
		// 	$("#successMessage").removeClass("btn-success");
		// 	$("#successMessage").addClass("btn-danger");
		// 	setTimeout(function(){ 
		// 		$("#successMessage").addClass("hide");
		// 		$("#successMessage").removeClass("btn-danger");
		// 		$("#successMessage").addClass("btn-success");
		// 	}, 3000);   
		// }

	});

});
function execute() {
	console.log("execute");
	let db = $("#joinDb").html();
	let projectionColumns = $("#inputJoinColumns").val();
	let tabel1 = $("#joinTable").html();
	let join = $("#selectJoinType").val();
	let tabel2 = $("#inputJoinTabel1").val();
	let condition1 = $("#inputCondition1").val();
	let tabel3 = $("#inputJoinTabel2").val();
	let condition2 = $("#inputCondition2").val();
	let groupby = $("#inputGroupBy").val();
	let having = $("#inputHaving").val();


	console.log(db);
	console.log(projectionColumns);
	console.log(tabel1);
	console.log(join);
	console.log(tabel2);
	console.log(condition1);
	console.log(tabel3);
	console.log(condition2);
	console.log(groupby);
	console.log(having);
	// $.ajax({
	// 	url: "http://127.0.0.1:8080/dbs/join",
	// 	type: "POST",
	// 	data: {
	// 		"database": db,
	// 		"projectionColumns":projectionColumns,
	// 		"table1": tabel1,
	// 		"table2": tabel2,
	// 		"table3": tabel3,
	// 		"condition1": condition1,
	// 		"condition2": condition2,
	// 		"groupby":groupby,
	// 		"having":having,
	// 		"joinType": join
	// 	}
	// })
	if (condition1 != "") {
		// $.ajax({
		// 	url: "http://127.0.0.1:8080/dbs/join",
		// 	type: "POST",
		// 	data: {
		// 		"database": db,
		// 		"table1": tableName,
		// 		"table2": selectedTabel,
		// 		"condition": condEgalitate,
		// 		"joinType": selectedJoin
		// 	},
		$.ajax({
			url: "http://127.0.0.1:8080/dbs/join",
			type: "POST",
			data: {
				"database": db,
				"projectionColumns": projectionColumns,
				"table1": tabel1,
				"table2": tabel2,
				"table3": tabel3,
				"condition1": condition1,
				"condition2": condition2,
				"groupby": groupby,
				"having": having,
				"joinType": join
			},
			success: function (response) {
				console.log(response);
				$(".form.show").removeClass("show");
				$("#formJoin").addClass("hide");
				$("#divForTable").removeClass("hide");
				$("#divForTable").addClass("show");
				$("#divForTable").html("");
				$("#divForTable").append(response);
			},
			error: function (response) {
				$("#successMessage").html(response.responseText);
				$("#successMessage").removeClass("hide");
				$("#successMessage").removeClass("btn-success");
				$("#successMessage").addClass("btn-danger");
				setTimeout(function () {
					$("#successMessage").addClass("hide");
					$("#successMessage").removeClass("btn-danger");
					$("#successMessage").addClass("btn-success");
				}, 3000);
			}

		});
	} else {
		$("#successMessage").html("Please make the selection!  ");
		$("#successMessage").removeClass("hide");
		$("#successMessage").removeClass("btn-success");
		$("#successMessage").addClass("btn-danger");
		setTimeout(function () {
			$("#successMessage").addClass("hide");
			$("#successMessage").removeClass("btn-danger");
			$("#successMessage").addClass("btn-success");
		}, 3000);
	}



}
function executeOuter() {
	let db = $("#outerDb").html();
	let projectionColumns = $("#inputOuterColumns").val();
	let tabel1 = $("#outerTable").html();
	let join = $("#selectOuterType").val();
	let tabel2 = $("#inputOuterTabel1").val();
	let condition1 = $("#inputConditionOuter").val();
	console.log(db);
	console.log(projectionColumns);
	console.log(tabel1);
	console.log(join);
	console.log(tabel2);
	console.log(condition1);

	if (condition1 != "") {
		$.ajax({
			url: "http://127.0.0.1:8080/dbs/outerjoin",
			type: "POST",
			data: {
				"database": db,
				"projectionColumns": projectionColumns,
				"table1": tabel1,
				"table2": tabel2,
				"condition1": condition1,
				"joinType": join
			},
			success: function (response) {
				console.log(response);
				$(".form.show").removeClass("show");
				$("#formOuter").addClass("hide");
				$("#divForTable").removeClass("hide");
				$("#divForTable").addClass("show");
				$("#divForTable").html("");
				$("#divForTable").append(response);
			},
			error: function (response) {
				$("#successMessage").html(response.responseText);
				$("#successMessage").removeClass("hide");
				$("#successMessage").removeClass("btn-success");
				$("#successMessage").addClass("btn-danger");
				setTimeout(function () {
					$("#successMessage").addClass("hide");
					$("#successMessage").removeClass("btn-danger");
					$("#successMessage").addClass("btn-success");
				}, 3000);
			}

		});
	} else {
		$("#successMessage").html("Please complete the fields corectly! ");
		$("#successMessage").removeClass("hide");
		$("#successMessage").removeClass("btn-success");
		$("#successMessage").addClass("btn-danger");
		setTimeout(function () {
			$("#successMessage").addClass("hide");
			$("#successMessage").removeClass("btn-danger");
			$("#successMessage").addClass("btn-success");
		}, 3000);
	}
}

function executeBonus() {
	let db = $("#bonusDb").html();
	let projectionColumns = $("#inputBonusColumns").val();
	let tabel1 = $("#bonusTable").html();
	let where = $("#bonusWhere").val();
	let type = $("#selectBonusType").val();
	let colTable2 = $("#inputBonusColTable").val();
	let tabel2 = $("#inputBonusTable").val();
	console.log(db);
	console.log(projectionColumns);
	console.log(tabel1);
	console.log(where);
	console.log(type);
	console.log(colTable2);
	console.log(tabel2);

	$.ajax({
		url: "http://127.0.0.1:8080/dbs/SAJoin",
		type: "POST",
		data: {
			"database": db,
			"projectionColumns": projectionColumns,
			"table1": tabel1,
			"where":where,
			"type":type,
			"colTabel2":colTable2,
			"table2": tabel2,
		},
		success: function (response) {
			console.log(response);
			$(".form.show").removeClass("show");
			$("#formBonus").addClass("hide");
			$("#divForTable").removeClass("hide");
			$("#divForTable").addClass("show");
			$("#divForTable").html("");
			$("#divForTable").append(response);
		},
		error: function (response) {
			$("#successMessage").html(response.responseText);
			$("#successMessage").removeClass("hide");
			$("#successMessage").removeClass("btn-success");
			$("#successMessage").addClass("btn-danger");
			setTimeout(function () {
				$("#successMessage").addClass("hide");
				$("#successMessage").removeClass("btn-danger");
				$("#successMessage").addClass("btn-success");
			}, 3000);
		}

	});

	// if (condition1 != "") {
	// 	$.ajax({
	// 		url: "http://127.0.0.1:8080/dbs/outerjoin",
	// 		type: "POST",
	// 		data: {
	// 			"database": db,
	// 			"projectionColumns": projectionColumns,
	// 			"table1": tabel1,
	// 			"table2": tabel2,
	// 			"condition1": condition1,
	// 			"joinType": join
	// 		},
	// 		success: function (response) {
	// 			console.log(response);
	// 			$(".form.show").removeClass("show");
	// 			$("#formOuter").addClass("hide");
	// 			$("#divForTable").removeClass("hide");
	// 			$("#divForTable").addClass("show");
	// 			$("#divForTable").html("");
	// 			$("#divForTable").append(response);
	// 		},
	// 		error: function (response) {
	// 			$("#successMessage").html(response.responseText);
	// 			$("#successMessage").removeClass("hide");
	// 			$("#successMessage").removeClass("btn-success");
	// 			$("#successMessage").addClass("btn-danger");
	// 			setTimeout(function () {
	// 				$("#successMessage").addClass("hide");
	// 				$("#successMessage").removeClass("btn-danger");
	// 				$("#successMessage").addClass("btn-success");
	// 			}, 3000);
	// 		}

	// 	});
	// } else {
	// 	$("#successMessage").html("Please complete the fields corectly! ");
	// 	$("#successMessage").removeClass("hide");
	// 	$("#successMessage").removeClass("btn-success");
	// 	$("#successMessage").addClass("btn-danger");
	// 	setTimeout(function () {
	// 		$("#successMessage").addClass("hide");
	// 		$("#successMessage").removeClass("btn-danger");
	// 		$("#successMessage").addClass("btn-success");
	// 	}, 3000);
}

function emptyColumns() {
	$("#inputTableDetailsName").val("");
	$("#inputTableDetailsType").val("");
	$("#inputTableDetailsLength").val("");
	$('input[type=checkbox]').prop('checked', false);
}
function showTables(dbName) {
	let className = ".tables" + dbName;
	if ($(className).length == 0) {
		$.ajax({
			url: "http://localhost:8080/dbs/getTables",
			type: "post",
			data: { database: dbName },
			success: function (response) {
				// console.log(response);   
				let dbs = "<ul class='tables" + dbName + " show'>";
				for (var i = 0; i < response.length; i++) {
					dbs += "<li id='btn" + response[i] + dbName + "' ><input class='btn btn-info' type='button' value='" + response[i] + "' onclick='showColumns(&quot;" + response[i] + "&quot;)'>" +
						"<button type='button' class='btn btn-link' style='color:red' onclick='deleteTable(&quot;" + response[i] + "&quot;,&quot;" + dbName + "&quot;)'>Delete</button> "
						+
						"<button type='button' class='btn btn-link' style='color:green' onclick='showIndexForm(&quot;" + response[i] + "&quot;,&quot;" + dbName + "&quot;)'>Add index</button> "
						+
						"<button type='button' class='btn btn-link' style='color:black' onclick='showInsertRecordForm(&quot;" + response[i] + "&quot;,&quot;" + dbName + "&quot;)'>Insert record</button> "
						+
						"<button type='button' class='btn btn-link' style='color:purple' onclick='showDeleteRecordForm(&quot;" + response[i] + "&quot;,&quot;" + dbName + "&quot;)'>Delete record</button> "
						+
						"<button type='button' class='btn btn-link' style='color:orange' onclick='showSelectForm(&quot;" + response[i] + "&quot;,&quot;" + dbName + "&quot;)'>Select</button> "
						+
						"<button type='button' class='btn btn-link' style='color:pink' onclick='showJoinForm(&quot;" + response[i] + "&quot;,&quot;" + dbName + "&quot;)'>Join</button> "
						+
						"<button type='button' class='btn btn-link' style='color:magenta' onclick='showOuterForm(&quot;" + response[i] + "&quot;,&quot;" + dbName + "&quot;)'>Outer Join</button> "
						+
						"<button type='button' class='btn btn-link' style='color:#ff9999' onclick='showBonusForm(&quot;" + response[i] + "&quot;,&quot;" + dbName + "&quot;)'>Semi&AntiJoin</button> "
						+
						"</li>";
				}
				dbs += "</ul>";
				let btn = "#btn" + dbName;
				$(btn).append(dbs);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
	}
	else {
		if ($(className).hasClass("show")) {
			$(className).removeClass("show");
			$(className).addClass("hide");
		} else {
			$(className).removeClass("hide");
			$(className).addClass("show");
		}
	}
}
function deleteDatabase(dbName) {
	if (confirm("Are you sure?")) {
		$.ajax({
			url: "http://localhost:8080/dbs/deleteDb",
			type: "POST",
			data: { "name": dbName },
			success: function (response) {
				let line = "#btn" + dbName;
				$(line).remove();
				// console.log($(line).parent().prop('className'));
				// console.log(response);          
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
		// your deletion code
	}
	return false;
}

function deleteTable(tableName, dbName) {
	if (confirm("Are you sure ?")) {
		$.ajax({
			url: "http://localhost:8080/dbs/deleteTable",
			type: "POST",
			data: {
				"tableName": tableName,
				"dbName": dbName
			},
			success: function (response) {
				let line = "#btn" + tableName + dbName;
				$(line).remove();
				console.log(response);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
		// your deletion code
	}
	return false;
}
function showFormFK() {
	if ($("#formFK").hasClass("show")) {
		$("#formFK").removeClass("show");
		$("#formFK").addClass("hide");
	} else {


		$("#formFK").removeClass("hide");
		$("#formFK").addClass("show");
		let db = $("#insertInDb").html();
		$.ajax({
			url: "http://localhost:8080/dbs/getTables",
			type: "post",
			data: { database: db },
			success: function (response) {
				// console.log(response);   
				$("#selectTable").empty();
				for (var i = 0; i < response.length; i++) {
					$("#selectTable").append("<option value='" + response[i] + "'>" + response[i] + "</option>");
				}

			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
	}


}
function showFormAddTable(database) {
	$("#insertInDb").html(database);
	console.log("intra");
	$(".form.show").removeClass("show");
	$(".form").addClass("hide");
	$("#formAddTable").removeClass("hide");
	$("#formAddTable").addClass("show");
}
function showIndexForm(tableName, dbName) {
	$("#createIndexInDatabase").html(dbName);
	$("#createIndexInTable").html(tableName);
	$(".form.show").removeClass("show");
	$(".form").addClass("hide");
	$("#formIndex").removeClass("hide");
	$("#formIndex").addClass("show");
	$.ajax({
		url: "http://localhost:8080/dbs/getFields",
		type: "post",
		data: {
			database: dbName,
			table: tableName
		},
		success: function (response) {
			// console.log(response);   
			$("#selectFields").empty();
			for (var i = 0; i < response.length; i++) {
				$("#selectFields").append("<option value='" + response[i] + "'>" + response[i] + "</option>");
			}

		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});
}

function showSelectForm(tableName, dbName) {
	$("#selectFromDatabase").html(dbName);
	$("#selectFromTable").html(tableName);
	$(".form.show").removeClass("show");
	$(".form").addClass("hide");
	$("#formSelect").removeClass("hide");
	$("#formSelect").addClass("show");
	$.ajax({
		url: "http://localhost:8080/dbs/getFields",
		type: "post",
		data: {
			database: dbName,
			table: tableName
		},
		success: function (response) {
			// console.log(response);   
			$("#selectProjectionFields").empty();
			for (var i = 0; i < response.length; i++) {
				$("#selectProjectionFields").append("<option value='" + response[i] + "'>" + response[i] + "</option>");
			}
			// $("#selectProjectionFields").append("<option value=\"*\">*</option>");

		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});
}
async function showInsertRecordForm(tableName, dbName) {
	$("#insertRecordInDatabase").html(dbName);
	$("#insertRecordInTable").html(tableName);
	$(".form.show").removeClass("show");
	$(".form").addClass("hide");
	$("#formInsertRecord").removeClass("hide");
	$("#formInsertRecord").addClass("show");

	var fk = [];
	await $.ajax({
		url: "http://localhost:8080/dbs/getFKs",
		type: "post",
		data: {
			database: dbName,
			table: tableName
		},
		success: function (response) {
			for (var i = 0; i < response.length; i++) {
				fk.push(response[i]);
			}

		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});

	var pk = [];
	await $.ajax({
		url: "http://localhost:8080/dbs/getPKs",
		type: "post",
		data: {
			database: dbName,
			table: tableName
		},
		success: function (response) {
			for (var i = 0; i < response.length; i++) {
				pk.push(response[i]);
			}

		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});
	console.log(pk);

	$.ajax({
		url: "http://localhost:8080/dbs/getFields",
		type: "post",
		data: {
			database: dbName,
			table: tableName
		},
		success: function (response) {
			$("#divForFields").html("");
			for (var i = 0; i < response.length; i++) {
				if (checkFK(fk, response[i]) != -1) {
					var j = checkFK(fk, response[i]);
					$("#divForFields").append(
						"<div class=\"form-group\">"
						+
						"<div class=\"col-sm-4\">"
						+
						"<label class='control-label lbl '> Foreign key: </label><input type=\"text\" style=\"margin-bottom:10px\" class=\"form-control inp\" value=\"" + fk[j] + "\" readonly ></div>"
						+
						// "<input type='text' style=\"margin-bottom:10px\" class=\"form-control inp\" id=\"input"+fk[j]+"insert\" ></div>"
						// +
						"<div class=\"col-sm-4\">"
						+
						"<label class='control-label lbl '> References to: </label><input type=\"text\" style=\"margin-bottom:10px\" class=\"form-control inp\" value=\"" + fk[j + 1] + "\" readonly ></div>"
						+
						"<div class=\"col-sm-4\">"
						+
						"<label class='control-label lbl'> Field: " + fk[j + 2] + " </label>"
						+
						"<input type='text' style=\"margin-bottom:10px\" class=\"form-control inp\" id=\"input" + fk[j + 2] + "insert\" ></div>"
						+
						"</div>");

					// e fk
				} else {
					if (checkPK(pk, response[i]) != -1) {
						$("#divForFields").append(
							"<label class='control-label col-sm-4 lbl'>pk " + response[i] + ": </label>" +
							"<div class='col-sm-8'><input type='text' style=\"margin-bottom:10px\" class=\"form-control inp\" id=\"input" + response[i] + "insert\" ></div>");
					} else {
						//adauga normal
						$("#divForFields").append(
							"<label class='control-label col-sm-4 lbl'>" + response[i] + ": </label>" +
							"<div class='col-sm-8'><input type='text' style=\"margin-bottom:10px\" class=\"form-control inp\" id=\"input" + response[i] + "insert\" ></div>");
					}
				}
			}

		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});

}

async function showDeleteRecordForm(tableName, dbName) {
	$("#deleteRecordInDatabase").html(dbName);
	$("#deleteRecordInTable").html(tableName);
	$(".form.show").removeClass("show");
	$(".form").addClass("hide");
	$("#formDeleteRecord").removeClass("hide");
	$("#formDeleteRecord").addClass("show");

	await $.ajax({
		url: "http://localhost:8080/dbs/getPKs",
		type: "post",
		data: {
			database: dbName,
			table: tableName
		},
		success: function (response) {
			$("#divForPKFields").html("");
			for (var i = 0; i < response.length; i++) {
				$("#divForPKFields").append(
					"<div class=\"form-group\">"
					+
					"<label class='control-label col-sm-4'>" + response[i] + ": </label><div class=\"col-sm-8\"><input type=\"text\" style=\"margin-bottom:10px\" class=\"form-control inpDel\" ></div></div>");
			}

		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});


}
function checkFK(array, fk) {
	var ret = -1;
	for (var i = 0; i < array.length; i = i + 3) {
		if (fk == array[i]) {
			ret = i;
			break;
		}
	}
	return ret;
}
function checkPK(array, pk) {
	var ret = -1;
	for (var i = 0; i < array.length; i = i + 1) {
		if (pk == array[i]) {
			ret = i;
			break;
		}
	}
	return ret;
}
function test() {
	$.ajax({
		url: "http://127.0.0.1:8080/dbs/test",
		type: "POST",
		data: { "name": "" }
		,
		success: function (response) {
			console.log(response);

		},
		error: function (response) {
			console.log(response.status);
			console.log(response.responseText)
		}

	});
}
var fksJoin = [];
var tabelJoin;
async function showJoinForm(tableName, dbName) {
	$("#joinTable").html(tableName);
	$("#joinDb").html(dbName);

	tabelJoin = tableName;
	$(".form.show").removeClass("show");
	$(".form").addClass("hide");
	$("#formJoin").removeClass("hide");
	$("#formJoin").addClass("show");
	fksJoin = [];
	// $("#condEgalitate").val("condition");

	// var fk=[];
	await $.ajax({
		url: "http://localhost:8080/dbs/getFKs",
		type: "post",
		data: {
			database: dbName,
			table: tableName
		},
		success: function (response) {
			for (var i = 0; i < response.length; i++) {
				fksJoin.push(response[i]);
			}

		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
		}
	});
	// $("#selectJoinTables").empty();
	// $("#selectJoinTables").append("<option> </option>");
	// for (var i=1;i<fksJoin.length;i+=3) {
	// 	$("#selectJoinTables").append("<option  value='"+fksJoin[i]+"'>"+fksJoin[i]+"</option>");
	// } 		
	console.log(fksJoin);

}
function showOuterForm(tableName, dbName) {
	$("#outerTable").html(tableName);
	$("#outerDb").html(dbName);

	$(".form.show").removeClass("show");
	$(".form").addClass("hide");
	$("#formOuter").removeClass("hide");
	$("#formOuter").addClass("show");
}
function showBonusForm(tableName, dbName) {
	$("#bonusTable").html(tableName);
	$("#bonusDb").html(dbName);
	$(".form.show").removeClass("show");
	$(".form").addClass("hide");
	$("#formBonus").removeClass("hide");
	$("#formBonus").addClass("show");
}
