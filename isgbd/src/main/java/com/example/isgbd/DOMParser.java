package com.example.isgbd;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DOMParser {

    public DOMParser() {

//        System.out.println(getFields("persoane","persoana"));
//        System.out.println(getPks("persoane","persoana"));
//        System.out.println(getFKs("persoane","persoana"));

//        System.out.println(getIndex("test","aaaa"));

    }


    public List<String> getDatabases(){
        List<String> databases=new ArrayList<>() ;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            NodeList dbs = doc.getElementsByTagName("DataBase");
            for (int i = 0; i < dbs.getLength(); i++) {
                Node node = dbs.item(i);
                Element e = (Element) node;
                databases.add(e.getAttribute("dataBaseName"));
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return databases;
    }
    public List<String> getTables(String database){
        List<String> getTables=new ArrayList<>() ;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            NodeList tableslist = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tableslist.getLength(); i++) {
                if(tableslist.item(i).getParentNode().getAttributes().getNamedItem("dataBaseName").getNodeValue().equals(database)){
                    NodeList  tables= tableslist.item(i).getChildNodes();
                    for (int j = 0; j < tables.getLength(); j++) {
                        Node table = tables.item(j);
                        if (table.getNodeType() == Node.ELEMENT_NODE) {
                            getTables.add(table.getAttributes().getNamedItem("tableName").getNodeValue());
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return getTables;
    }
    public void deleteTable(String database,String name) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            NodeList tables = doc.getElementsByTagName("Tables");
            System.out.println(tables.getLength());
            for(int i=0;i<tables.getLength();i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    System.out.println("copii" + tabele.getLength());
                    for(int j=0;j<tabele.getLength();j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            System.out.println(tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue());
                            if(tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)){
                                tables.item(i).removeChild(tabele.item(j));
                            }
                        }
                    }
                }
            }
            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StreamResult result = new StreamResult(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml"));
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public void addTable(String database,String nume, String rowlength, List<String> campuri,List<String> fk) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            NodeList dbs = doc.getElementsByTagName("Tables");
            System.out.println(dbs.getLength());
            for (int i = 0; i < dbs.getLength(); i++) {
                Node node = dbs.item(i);
                Element e = (Element) node;
                Node parent = e.getParentNode();
                Element parinte = (Element) parent;
                if (parinte.getAttribute("dataBaseName").equals(database)) {
                    //am gasit tables in care trebuie adaugat
//                    elementul de adaugat
                    Element el = doc.createElement("Table");
                    el.setAttribute("tableName", nume);
                    el.setAttribute("fileName", nume + ".kv");
                    el.setAttribute("rowLength", rowlength);

                    Element structure = doc.createElement("Structure");
                    for (int k = 0; k < campuri.size(); k += 5) {
                        Element atribut = doc.createElement("Attribute");
                        atribut.setAttribute("attributeName", campuri.get(k));
                        atribut.setAttribute("type", campuri.get(k + 1));
                        atribut.setAttribute("length", campuri.get(k + 2));
                        atribut.setAttribute("isNull", campuri.get(k + 3));
                        structure.appendChild(atribut);
                    }
                    el.appendChild(structure);
                    Element primaryKey = doc.createElement("primaryKey");
                    List<String> pKeys=new ArrayList<>();
                    for (int k = 0; k < campuri.size(); k += 5) {
                        if (campuri.get(k + 4).equals("1")) {
                            Element pk = doc.createElement("pkAttribute");
                            pKeys.add(campuri.get(k));
                            pk.appendChild(doc.createTextNode(campuri.get(k)));
                            primaryKey.appendChild(pk);
                        }
                    }
                    el.appendChild(primaryKey);

                    Element foreignKeys = doc.createElement("foreignKeys");
                    for (int k = 0; k < fk.size(); k += 3) {
                            Element foreignKey=doc.createElement("foreignKey");
                            Element fkAttribute = doc.createElement("fkAttribute");
                            fkAttribute.appendChild(doc.createTextNode(fk.get(k)));
                            foreignKey.appendChild(fkAttribute);
                            Element references=doc.createElement("references");
                            Element refTable=doc.createElement("refTable");
                            refTable.appendChild(doc.createTextNode(fk.get(k+1)));
                            Element refAttribute=doc.createElement("refAttribute");
                            refAttribute.appendChild(doc.createTextNode(fk.get(k+2)));
                            references.appendChild(refTable);
                            references.appendChild(refAttribute);
                            foreignKey.appendChild(references);

                        foreignKeys.appendChild(foreignKey);
                    }
                    el.appendChild(foreignKeys);


                    Element indexFiles = doc.createElement("IndexFiles");
                    Element indexFile=doc.createElement("IndexFile");
                    indexFile.setAttribute("keyLength","25");
                    indexFile.setAttribute("isUnique","true");
                    indexFile.setAttribute("indexType","BTree");
                    indexFile.setAttribute("indexName",nume+".ind");
                    Element indexAttributes=doc.createElement("IndexAttributes");

                    System.out.println(pKeys);
                    for (String pk:pKeys
                         ) {
                        Element iAttribute=doc.createElement("IAttribute");
                        iAttribute.appendChild(doc.createTextNode(pk));
                        indexAttributes.appendChild(iAttribute);
                    }
                    indexFile.appendChild(indexAttributes);
                    indexFiles.appendChild(indexFile);
                    for (int k = 0; k < fk.size(); k += 3) {
                        Element fkindex =  doc.createElement("IndexFile");
                        fkindex.setAttribute("keyLength","25");
                        fkindex.setAttribute("isUnique","false");
                        fkindex.setAttribute("indexType","BTree");
                        fkindex.setAttribute("indexName",database+nume+fk.get(k));
                        Element iAttributes=doc.createElement("IndexAttributes");
                        Element iAttribute=doc.createElement("IAttribute");
                        iAttribute.appendChild(doc.createTextNode(fk.get(k)));
                        iAttributes.appendChild(iAttribute);
                        fkindex.appendChild(iAttributes);
                        indexFiles.appendChild(fkindex);
                    }

                    el.appendChild(indexFiles);

                    e.appendChild(el); //e este tables
                }
            }
            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StreamResult result = new StreamResult(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml"));
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public void createDB(String nume) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            Element rootElement = doc.getDocumentElement();

            Element db = doc.createElement("DataBase");
            db.setAttribute("dataBaseName", nume);

            Element n = doc.createElement("Tables");
            db.appendChild(n);
            rootElement.appendChild(db);

            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StreamResult result = new StreamResult(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml"));
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public void deleteDB(String nume) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            Element rootElement = doc.getDocumentElement();
            NodeList dbs = doc.getElementsByTagName("DataBase");
            for (int i = 0; i < dbs.getLength(); i++) {
                Node node = dbs.item(i);
                Element e = (Element) node;
                String numedb = e.getAttribute("dataBaseName");
                if (numedb.equals(nume)) {
                    rootElement.removeChild(node);
                    System.out.println("Sters");
                }
            }
            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StreamResult result = new StreamResult(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml"));
            transformer.transform(source, result);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public boolean checkDb(String database){
        List<String> dbs=getDatabases();
        if(dbs.contains(database))
            return true;
        return false;
    }
    public boolean checkTable(String database,String table){
        List<String> tables=getTables(database);
        if(tables.contains(table))
            return true;
        return false;
    }
    public List<String> getFields(String database,String name) {
        List<String> fields=new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            NodeList tables = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tables.getLength(); i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)) {
                                NodeList childs=tabele.item(j).getChildNodes();
                                for(int k=0;k<childs.getLength();k++){
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {
                                        if(c.getNodeName().equals("Structure")){
                                            NodeList attributes=c.getChildNodes();
                                            for(int x=0;x<attributes.getLength();x++){
                                                Node a=attributes.item(x);
                                                if(a.getNodeType()==Node.ELEMENT_NODE){
                                                    fields.add(a.getAttributes().getNamedItem("attributeName").getNodeValue());

                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
            return fields;
        }

    public void addIndex(String database,String tableName,String indexName,String isUnique,List<String> fields) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            NodeList tables = doc.getElementsByTagName("Tables");
            System.out.println(tables.getLength());
            for(int i=0;i<tables.getLength();i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(tableName)) {
                                NodeList childs = tabele.item(j).getChildNodes();
                                for (int k = 0; k < childs.getLength(); k++) {
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {
                                        if (c.getNodeName().equals("IndexFiles")) {
                                            Element indexFile=doc.createElement("IndexFile");
                                            indexFile.setAttribute("keyLength","25");
                                            indexFile.setAttribute("isUnique",isUnique);
                                            indexFile.setAttribute("indexType","BTree");
                                            indexFile.setAttribute("indexName",indexName);
                                            Element indexAttributes=doc.createElement("IndexAttributes");
                                            for (String f:fields
                                                    ) {
                                                Element iAttribute=doc.createElement("IAttribute");
                                                iAttribute.appendChild(doc.createTextNode(f));
                                                indexAttributes.appendChild(iAttribute);
                                            }
                                            indexFile.appendChild(indexAttributes);
                                            c.appendChild(indexFile);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();

            StreamResult result = new StreamResult(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml"));
            transformer.transform(source, result);


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }


    public List<String> getFKs(String database,String name) {
        List<String> fields=new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            NodeList tables = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tables.getLength(); i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)) {
                                NodeList childs=tabele.item(j).getChildNodes();
                                for(int k=0;k<childs.getLength();k++){
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {

                                        if(c.getNodeName().equals("foreignKeys")){
                                            NodeList attributes=c.getChildNodes();
                                            for(int x=0;x<attributes.getLength();x++){
                                                Node a=attributes.item(x);
                                                if(a.getNodeType()==Node.ELEMENT_NODE){
                                                    NodeList fkProp=a.getChildNodes();
                                                    for(int m=0;m<fkProp.getLength();m++){
                                                        Node z=fkProp.item(m);
                                                        if(z.getNodeType()==Node.ELEMENT_NODE &&z.getNodeName().equals("fkAttribute"))
                                                            fields.add(z.getTextContent());
                                                        if(z.getNodeType()==Node.ELEMENT_NODE &&z.getNodeName().equals("references")){
                                                            NodeList refList=z.getChildNodes();
                                                            for(int n=0;n<refList.getLength();n++){
                                                                Node ref=refList.item(n);
                                                                if(ref.getNodeType()==Node.ELEMENT_NODE){
                                                                    fields.add(ref.getTextContent());
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fields;
    }


    public List<String> getPks(String database,String name) {
        List<String> fields=new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            NodeList tables = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tables.getLength(); i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)) {
                                NodeList childs=tabele.item(j).getChildNodes();
                                for(int k=0;k<childs.getLength();k++){
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {

                                        if(c.getNodeName().equals("primaryKey")){
                                            NodeList attributes=c.getChildNodes();
                                            for(int x=0;x<attributes.getLength();x++){
                                                Node a=attributes.item(x);
                                                if(a.getNodeType()==Node.ELEMENT_NODE){
                                                    fields.add(a.getTextContent());
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fields;
    }

    public List<IndexFile> getIndex(String database,String name) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        List<IndexFile> toReturn=new ArrayList<>();
        String indexName="";
        String isUnique="";
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs.xml");
            NodeList tables = doc.getElementsByTagName("Tables");
            for (int i = 0; i < tables.getLength(); i++) {
                Node db = tables.item(i).getParentNode();
                Element dbase = (Element) db;
                if (dbase.getAttribute("dataBaseName").equals(database)) {
                    NodeList tabele = tables.item(i).getChildNodes();
                    for (int j = 0; j < tabele.getLength(); j++) {
                        Node tabel = tabele.item(j);
                        if (tabel.getNodeType() == Node.ELEMENT_NODE) {
                            if (tabele.item(j).getAttributes().getNamedItem("tableName").getNodeValue().equals(name)) {
                                NodeList childs=tabele.item(j).getChildNodes();
                                for(int k=0;k<childs.getLength();k++){
                                    Node c = childs.item(k);
                                    if (c.getNodeType() == Node.ELEMENT_NODE) {
                                        if(c.getNodeName().equals("IndexFiles")){
                                            NodeList indecsi=c.getChildNodes();
                                            for(int x=0;x<indecsi.getLength();x++){
                                                Node index=indecsi.item(x);
                                                if(index.getNodeType()==Node.ELEMENT_NODE){
                                                    if(index.getNodeName().equals("IndexFile")){
                                                        indexName=index.getAttributes().getNamedItem("indexName").getNodeValue();
                                                        isUnique=index.getAttributes().getNamedItem("isUnique").getNodeValue();
                                                        NodeList indexAttributes=index.getChildNodes();
                                                        for(int m=0;m<indexAttributes.getLength();m++){
                                                            Node ia=indexAttributes.item(m);
                                                            if(ia.getNodeType()==Node.ELEMENT_NODE) {
                                                                if (ia.getNodeName().equals("IndexAttributes")) {
                                                                    NodeList iAttributes = ia.getChildNodes();
                                                                    List<String> fields=new ArrayList<>();
                                                                    for (int n = 0; n < iAttributes.getLength(); n++) {
                                                                        Node iAtr = iAttributes.item(n);

                                                                        if (iAtr.getNodeType() == Node.ELEMENT_NODE && iAtr.getNodeName().equals("IAttribute")) {
                                                                            fields.add(iAtr.getTextContent());
                                                                            //ia valoarea
                                                                        }

                                                                    }
                                                                    toReturn.add(new IndexFile(indexName, fields, database, name, isUnique));
                                                                }
                                                            }

                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return toReturn;
    }
}
