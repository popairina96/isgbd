package com.example.isgbd;

import com.example.isgbd.KeyValueSystem.Condition;
import com.example.isgbd.KeyValueSystem.ServiceBerkley;
import com.sleepycat.je.Database;
import com.sleepycat.je.Environment;
import org.aspectj.lang.reflect.FieldSignature;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dbs")
public class Controller {
    public DOMParser domParser;
    public ServiceBerkley serviceBerkley;
    public List<IndexFile> indecsi;

    public Controller() {
        domParser = new DOMParser();
        serviceBerkley = new ServiceBerkley();
        indecsi = new ArrayList<>();
    }

    //incercari de adaugare
//    public static void workWithXML() {
//        try {
//            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
//
//            // root elements
//            Document doc = docBuilder.newDocument();
//            Element rootElement = doc.createElement("company");
//            doc.appendChild(rootElement);
//
//            Element studenti = doc.createElement("studenti");
//            rootElement.appendChild(studenti);
//
//            Element student1 = doc.createElement("student");
//            studenti.appendChild(student1);
//
//            Element firstname1 = doc.createElement("firstname");
//            firstname1.appendChild(doc.createTextNode("primul"));
//            student1.appendChild(firstname1);
//
//            //
//            Element student2 = doc.createElement("student");
//            studenti.appendChild(student2);
//
//            Element firstname2 = doc.createElement("firstname");
//            firstname2.appendChild(doc.createTextNode("al doilea"));
//            student2.appendChild(firstname2);
//
//            TransformerFactory transformerFactory = TransformerFactory.newInstance();
//            Transformer transformer = transformerFactory.newTransformer();
//            DOMSource source = new DOMSource(doc);
//            StreamResult result = new StreamResult(new File("C:\\Users\\Irina\\workingwithxml\\src\\file.xml"));
//            transformer.transform(source, result);
//            System.out.println("File saved!");
//
//        } catch (ParserConfigurationException pce) {
//            pce.printStackTrace();
//        } catch (TransformerException tfe) {
//            tfe.printStackTrace();
//        }
//    }


    @CrossOrigin
    @RequestMapping(value = "/getDbs", method = RequestMethod.GET)
    public List<String> findAllDatabases() {
        return domParser.getDatabases();
    }

    @CrossOrigin
    @RequestMapping(value = "/getTables", method = RequestMethod.POST)
    public List<String> getTables(@RequestParam String database) {
        return domParser.getTables(database);
    }

    @CrossOrigin
    @RequestMapping(value = "/getFields", method = RequestMethod.POST)
    public List<String> getTables(@RequestParam String database, @RequestParam String table) {
        System.out.println(domParser.getFields(database, table));
        return domParser.getFields(database, table);
    }

    @CrossOrigin
    @RequestMapping(value = "/getFKs", method = RequestMethod.POST)
    public List<String> getFKs(@RequestParam String database, @RequestParam String table) {
        return domParser.getFKs(database, table);
    }

    @CrossOrigin
    @RequestMapping(value = "/getPKs", method = RequestMethod.POST)
    public List<String> getPKs(@RequestParam String database, @RequestParam String table) {
        return domParser.getPks(database, table);
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> addDb(@RequestParam String name) {
        if (!domParser.checkDb(name)) {
            domParser.createDB(name);
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("This database already exist!", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/test", method = RequestMethod.POST)
    public ResponseEntity<?> test(@RequestParam String name) {
        System.out.println(name);
        if (!name.isEmpty()) {
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("This database already exist!", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/deleteDb", method = RequestMethod.POST)
    public ResponseEntity<?> deleteDB(@RequestParam String name) {
        System.out.println(name);
        if (!name.isEmpty()) {
            domParser.deleteDB(name);
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/deleteTable", method = RequestMethod.POST)
    public ResponseEntity<?> deleteTable(@RequestParam String tableName, @RequestParam String dbName) {
        if (!tableName.isEmpty() && !dbName.isEmpty()) {
            domParser.deleteTable(dbName, tableName);
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/addTable", method = RequestMethod.POST)
    public ResponseEntity<?> addTable(@RequestParam String database, @RequestParam String table, @RequestParam String rowLength, @RequestParam List<String> fields, @RequestParam List<String> foreignKeys) {
//        System.out.println(table);
//        System.out.println(rowLength);
//        System.out.println(database);
//        System.out.println(fields);
//        System.out.println("foreign keys: "+ foreignKeys);

//        if (!database.isEmpty()&&!table.isEmpty()&&!rowLength.isEmpty()&&!fields.isEmpty()) {
        if (!domParser.checkTable(database, table)) {
            domParser.addTable(database, table, rowLength, fields, foreignKeys);
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/addIndex", method = RequestMethod.POST)
    public ResponseEntity<String> addIndex(@RequestParam String database, @RequestParam String table, @RequestParam String indexName, @RequestParam String isUnique, @RequestParam List<String> fields) {
//        System.out.println(table);
//        System.out.println(database);
//        System.out.println(indexName);
//        System.out.println(isUnique);
//        System.out.println(fields);
        if (!database.isEmpty() && !table.isEmpty() && !fields.isEmpty()) {
            domParser.addIndex(database, table, indexName, isUnique, fields);
//            indecsi.add(new IndexFile(indexName,fields.get(0),database,table,domParser.getPks(database,table),isUnique));

            //creeaza indexfile
            return new ResponseEntity<String>("ok", HttpStatus.OK);
        } else {
            return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/selectStatement", method = RequestMethod.POST)
    public ResponseEntity<String> selectStatement(@RequestParam String database, @RequestParam String table, @RequestParam String condition, @RequestParam List<String> fields) {
        System.out.println(table);
        System.out.println(database);
        System.out.println(condition);
        System.out.println(fields);
        List<String> coloaneTabel = domParser.getFields(database, table);
        List<String> columns = new ArrayList<>();
        List<Condition> conditions = new ArrayList<>();
        //where:   c1>2 && c2<3
        if (!database.isEmpty() && !table.isEmpty() && !fields.isEmpty()) {
            if (fields.size() > 1 && fields.contains("*")) {
                return new ResponseEntity<String>("Select all the column or only the * !", HttpStatus.BAD_REQUEST);
            }
            String[] cond = condition.split(" ");
            List<IndexFile> indecsiExistenti = domParser.getIndex(database, table);
            if (cond.length == 1) {
                List<String> parsedCond = parseazaCond(cond[0]);
                conditions.add(new Condition(parsedCond.get(0), parsedCond.get(1), parsedCond.get(2)));
                columns.add(parsedCond.get(0));
            } else {
                for (int i = 0; i < cond.length; i++) {
                    if (!cond[i].contains("&&")) {
                        List<String> parsedCond = parseazaCond(cond[i]);
                        conditions.add(new Condition(parsedCond.get(0), parsedCond.get(1), parsedCond.get(2)));
                        columns.add(parsedCond.get(0));
                    }
                }
            }

            if (coloaneTabel.containsAll(columns)) {
                IndexFile rez = getSpecificIndex(indecsiExistenti, columns);
                if (rez != null) {
                    domParser.getPks(database, table);
                    String rezultat = serviceBerkley.cauta(conditions, rez, domParser.getPks(database, table), fields, domParser.getFields(database, table));
                    return new ResponseEntity<String>(rezultat, HttpStatus.OK);


                } else {
                    return new ResponseEntity<String>("There is no index on the condition columns!", HttpStatus.BAD_REQUEST);
                }
            } else {
                return new ResponseEntity<String>("Column/s doesnt exist!", HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
        }
    }


    @CrossOrigin
    @RequestMapping(value = "/insertRecord", method = RequestMethod.POST)
    public ResponseEntity<String> insertRecord(@RequestParam String database, @RequestParam String table, @RequestParam List<String> fields) {
//        System.out.println(table);
//        System.out.println(database);
//        System.out.println(fields);

        //primeste lista cu tot cu label-uri
        if (!database.isEmpty() && !table.isEmpty() && !fields.isEmpty()) {
            int rez = serviceBerkley.adaugaRecord(database, table, fields);
            if (rez == 1) {
                //verifica daca exista index pe database+table respectiv si insereaza si acolo
                List<IndexFile> indecsiExistenti = domParser.getIndex(database, table);
                if (!indecsiExistenti.isEmpty()) {
                    for (IndexFile i : indecsiExistenti
                            ) {
                        if (!getPKs(database, table).equals(i.getFields())) {
                            serviceBerkley.adaugaInIndex(i, fields);
                        }
                    }
                }
                return new ResponseEntity<String>("ok", HttpStatus.OK);

            } else {
                if (rez == -1) {
                    return new ResponseEntity<String>("There is already a record with given id!", HttpStatus.BAD_REQUEST);
                } else {
                    return new ResponseEntity<String>("There is no record for the external key!", HttpStatus.BAD_REQUEST);
                }
            }

        } else {
            return new ResponseEntity<String>("Empty fields!", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/deleteRecord", method = RequestMethod.POST)
    public ResponseEntity<String> deleteRecord(@RequestParam String database, @RequestParam String table, @RequestParam List<String> values) {
        System.out.println(table);
        System.out.println(database);
        System.out.println(values);
        //primeste lista cu tot cu label-uri
        if (!database.isEmpty() && !table.isEmpty() && !values.isEmpty()) {
            if (verificaStergere(database, table, values) == true) {
                String rezStergere = serviceBerkley.deleteRecord(database, table, values);
                if (rezStergere != "") {

                    List<IndexFile> indecsiExistenti = domParser.getIndex(database, table);
                    if (!indecsiExistenti.isEmpty()) {
                        for (IndexFile i : indecsiExistenti
                                ) {
                            if (!getPKs(database, table).equals(i.getFields())) {
                                serviceBerkley.deleteFromIndex(i, rezStergere, domParser.getFields(database, table), domParser.getPks(database, table));
                            }
                        }
                    }
                    return new ResponseEntity<String>("ok", HttpStatus.OK);

                } else {
                    return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
                }
            } else {
                return new ResponseEntity<String>("There is a record in another table with this fk set!", HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<String>("no", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "/join", method = RequestMethod.POST)
//    public ResponseEntity<String> join(@RequestParam String database, @RequestParam String table1, @RequestParam String table2, @RequestParam String condition, @RequestParam String joinType) {
    public ResponseEntity<String> join(@RequestParam String database, @RequestParam String projectionColumns, @RequestParam String table1, @RequestParam String table2, @RequestParam String table3, @RequestParam String condition1, @RequestParam String condition2, @RequestParam String groupby, @RequestParam String having, @RequestParam String joinType) {
        if (joinType.equals("Hash")) {
            System.out.println("cond1" + condition1);
            return new ResponseEntity<String>(serviceBerkley.hashJoin(database, having, projectionColumns, condition1, condition2, groupby), HttpStatus.OK);
        }
        List<IndexFile> indecsiExistenti = domParser.getIndex(database, table1);
        List<String> columns1 = new ArrayList<>();
        String[] cond = condition1.split("=");
        System.out.println(condition1);
        String field1 = cond[0].split("\\.")[1];
        String field2 = cond[1].split("\\.")[1];
        columns1.add(field1);
        System.out.println(field1);
        System.out.println(field2);
        System.out.println(table1);
        System.out.println(table2);
        IndexFile indexTable1 = getSpecificIndex(indecsiExistenti, columns1);
        //tine loc de t1
//        String db="Shop";
//        String t1="Magazin";
//        String t2="Angajati";
//        List<IndexFile> indecsiExistenti = domParser.getIndex(db, t2);
//        List<String> coloane=new ArrayList<>();
//        coloane.add("idMagazin");
//        System.out.println(getSpecificIndex(indecsiExistenti,coloane));
//        serviceBerkley.indexNLJ(db,t1,domParser.getFields(db,t1),domParser.getPks(db,t1),domParser.getFields(db,t2),
//                domParser.getPks(db,t2),getSpecificIndex(indecsiExistenti,coloane),"idMag","idMagazin");

        if (indexTable1 != null) {
            String rez = serviceBerkley.indexNLJ(database, table2, domParser.getFields(database, table2), domParser.getPks(database, table2), domParser.getFields(database, table1),
                    domParser.getPks(database, table1), indexTable1, field2, field1);
            return new ResponseEntity<String>(rez, HttpStatus.OK);
        } else {
            //apeleaza index pt indexul pe a doua tabela

            //nu exista index pe coloana primului tabel, verificam pt cel de-al doilea tabel
            indecsiExistenti = domParser.getIndex(database, table2);
            List<String> columns2 = new ArrayList<>();
            columns2.add(field2);
            IndexFile indexTable2 = getSpecificIndex(indecsiExistenti, columns2);
            if (indexTable2 == null) {
                return new ResponseEntity<String>("There is no index on the selected columns!", HttpStatus.BAD_REQUEST);
            } else {
                // apeleaza index pt indexul pe prima tabela
                String rez = serviceBerkley.indexNLJ(database, table1, domParser.getFields(database, table1), domParser.getPks(database, table1), domParser.getFields(database, table2),
                        domParser.getPks(database, table2), indexTable2, field1, field2);
                return new ResponseEntity<String>(rez, HttpStatus.OK);

            }
        }
    }


    @CrossOrigin
    @RequestMapping(value = "/outerjoin", method = RequestMethod.POST)
    public ResponseEntity<String> outerJoin(@RequestParam String database, @RequestParam String projectionColumns, @RequestParam String table1, @RequestParam String table2, @RequestParam String condition1, @RequestParam String joinType) {
        if (joinType.equals("LEFT OUTER JOIN")) {
            return new ResponseEntity<String>(serviceBerkley.leftJoin(projectionColumns, database, table1, table2, condition1), HttpStatus.OK);
        } else if (joinType.equals("RIGHT OUTER JOIN")) {
            return new ResponseEntity<String>(serviceBerkley.rightJoin(projectionColumns, database, table1, table2, condition1), HttpStatus.OK);
        } else if (joinType.equals("FULL OUTER JOIN")) {
            return new ResponseEntity<String>(serviceBerkley.fullJoin(projectionColumns, database, table1, table2, condition1), HttpStatus.OK);

        }
        return new ResponseEntity<String>("Check the inputs!", HttpStatus.BAD_REQUEST);
    }

    @CrossOrigin
    @RequestMapping(value = "/SAJoin", method = RequestMethod.POST)
    public ResponseEntity<String> semiAndAntiJoin(@RequestParam String database, @RequestParam String projectionColumns, @RequestParam String table1, @RequestParam String where, @RequestParam String type, @RequestParam String colTabel2, @RequestParam String table2) {
        String[] col = projectionColumns.split(",");
        List<String> projcol=new ArrayList<>();
        for (int i = 0; i < col.length; i++) {
            if (!col[i].split("\\.")[0].equals(table1)) {
                return new ResponseEntity<String>("Cannot apply semi/anti join when columns from table 2 appear in projection", HttpStatus.BAD_REQUEST);
            }
            projcol.add(col[i]);
        }
        if (type.equals("IN")) {
            return new ResponseEntity<String>(serviceBerkley.semiJoin(projcol,table1,where,type,colTabel2,table2,database), HttpStatus.OK);
        } else if(type.equals("NOT IN")) {
            return new ResponseEntity<String>(serviceBerkley.antiJoin(projcol,table1,where,type,colTabel2,table2,database), HttpStatus.OK);

        }
        return new ResponseEntity<String>("Check the inputs!", HttpStatus.BAD_REQUEST);
    }


    public List<IndexFile> getIndex(String database, String table) {
        List<IndexFile> toReturn = new ArrayList<>();
        for (IndexFile i : indecsi
                ) {
            if (i.getDb().equals(database) && i.getTable().equals(table)) {
                toReturn.add(i);
            }
        }
        return toReturn;

    }

    public List<String> parseazaCond(String cond) {
        List<String> toReturn = new ArrayList<>();
        String[] split = new String[10];
        String c = "";
        if (cond.contains("!=")) {
            split = cond.split("!=");
            c = "!=";
        } else if (cond.contains(">=")) {
            split = cond.split("\\>=");
            c = ">=";
        } else if (cond.contains("<=")) {
            split = cond.split("<=");
            c = "<=";
        } else if (cond.contains("=")) {
            split = cond.split("=");
            c = "=";
        } else if (cond.contains("<")) {
            split = cond.split("<");
            c = "<";
        } else if (cond.contains(">")) {
            split = cond.split(">");
            c = ">";
        }
        toReturn.add(split[0]);
        toReturn.add(c);
        toReturn.add(split[1]);

        return toReturn;
    }

    public IndexFile getSpecificIndex(List<IndexFile> indecsi, List<String> columns) {

        for (IndexFile i : indecsi
                ) {
            if (i.getFields().containsAll(columns)) {
                return i;
            }

        }
        return null;
    }

    public boolean verificaStergere(String db, String actualTable, List<String> values) {
        List<String> tables = domParser.getTables(db);
        String id = "";
        for (String s : values
                ) {
            if (id.equals("")) {
                id += s;
            } else {
                id += "#" + s;
            }
        }
        for (String table : tables
                ) {
            List<String> fks = domParser.getFKs(db, table);
            for (int i = 1; i < fks.size(); i += 3) {
                if (fks.get(i).equals(actualTable)) {
                    //inseamna ca tabelul table are o cheie straina pe tabelul actual
                    String dbname = db + table + fks.get(i - 1);
                    String rez = serviceBerkley.getRecord(dbname, id);
                    if (rez != "") {
                        return false;
                    }
                    //exista un index pe db+table+fks.get(i-1);
                    //cauta daca exista record in bd
                }
            }

        }

        return true;
    }

}
