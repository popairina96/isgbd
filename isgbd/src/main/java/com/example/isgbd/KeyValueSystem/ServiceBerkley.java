package com.example.isgbd.KeyValueSystem;

import com.example.isgbd.DOMParser;
import com.example.isgbd.IndexFile;
import com.sleepycat.je.*;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.stream.Collectors;

public class ServiceBerkley {
    public MyDbEnv myDbEnv;
    public Environment env;
    public DOMParser domParser;
    private HashMap<Integer, String> hmap2;

    public MyDbEnv getMyDbEnv() {
        return myDbEnv;
    }

    public void setMyDbEnv(MyDbEnv myDbEnv) {
        this.myDbEnv = myDbEnv;
    }

    public Environment getEnv() {
        return env;
    }

    public void setEnv(Environment env) {
        this.env = env;
    }

    public ServiceBerkley() {
        myDbEnv = new MyDbEnv();
        domParser = new DOMParser();
        myDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
        env = myDbEnv.getEnvironment();
        //left join works ok
//        leftJoin("Magazin.idMag,Magazin.nume,Angajati.idAng,Angajati.nume", "Shop", "Magazin", "Angajati", "Angajati.idMagazin = Magazin.idMag");

        System.out.println("LEFT JOIN");
        leftJoin("Customers.idCustomer,Customers.name,Orders.idOrder,Orders.date", "Shop2", "Customers", "Orders", "Orders.idCustomer = Customers.idCustomer");

//        System.out.println("tabele interschimbate NU FUNCTIONEAZA // TREBUIE RIGHT JOIN");
//        leftJoin("Customers.idCustomer,Customers.name,Orders.idOrder,Orders.date", "Shop2", "Orders", "Customers", "Customers.idCustomer = Orders.idCustomer");

        System.out.println("RIGHT JOIN");
        rightJoin("Customers.idCustomer,Customers.name,Orders.idOrder,Orders.date", "Shop2", "Customers", "Orders", "Customers.idCustomer = Orders.idCustomer");

        System.out.println("FULL JOIN");
        fullJoin("Customers.idCustomer,Customers.name,Orders.idOrder,Orders.date", "Shop2", "Customers", "Orders", "Customers.idCustomer = Orders.idCustomer");


        //semi join fara index pe tabela2
//        System.out.println();
//        System.out.println("----------------------------");
//        System.out.println("SEMI JOIN");
//        List<String> projectionColumns=new ArrayList<>();
//        projectionColumns.add("Customers.idCustomer");
//        projectionColumns.add("Customers.name");
//        String tabel1="Customers";
//        String tabel2="Orders";
//        String where="Customers.idCustomer";
//        String operator="IN";
//        String colTabel2="Orders.idCustomer";
//        String db="Shop2";
//        semiJoin(projectionColumns,tabel1,where,operator,colTabel2,tabel2,db);

        //semi join cu cautare in index
//        System.out.println("Magazin-------------------");
//        afiseazaLista(getAllRecords("ShopMagazin"));
//        System.out.println("Angajati---------------------------");
//        afiseazaLista(getAllRecords("ShopAngajati"));
//
//        System.out.println();
//        System.out.println("----------------------------");
//        System.out.println("SEMI JOIN");
//        List<String> projectionColumns=new ArrayList<>();
//        projectionColumns.add("Magazin.idMag");
//        projectionColumns.add("Magazin.nume");
//        String tabel1="Magazin";
//        String tabel2="Angajati";
//        String where="Magazin.idMag";
//        String operator="IN";
//        String colTabel2="Angajati.idMagazin";
//        String db="Shop";
//        semiJoin(projectionColumns,tabel1,where,operator,colTabel2,tabel2,db);


        //anti join fara index
//        System.out.println();
//        System.out.println("----------------------------");
//        System.out.println("ANTI JOIN");
//        List<String> projectionColumns = new ArrayList<>();
//        projectionColumns.add("Customers.idCustomer");
//        projectionColumns.add("Customers.name");
//        String tabel1 = "Customers";
//        String tabel2 = "Orders";
//        String where = "Customers.idCustomer";
//        String operator = "IN";
//        String colTabel2 = "Orders.idCustomer";
//        String db = "Shop2";
//        antiJoin(projectionColumns, tabel1, where, operator, colTabel2, tabel2, db);

        //anti join cu cautare in index
        System.out.println("Magazin-------------------");
        afiseazaLista(getAllRecords("ShopMagazin"));
        System.out.println("Angajati---------------------------");
        afiseazaLista(getAllRecords("ShopAngajati"));

        System.out.println();
        System.out.println("----------------------------");
        System.out.println("SEMI JOIN");
        List<String> projectionColumns = new ArrayList<>();
        projectionColumns.add("Magazin.idMag");
        projectionColumns.add("Magazin.nume");
        String tabel1 = "Magazin";
        String tabel2 = "Angajati";
        String where = "Magazin.idMag";
        String operator = "IN";
        String colTabel2 = "Angajati.idMagazin";
        String db = "Shop";
        antiJoin(projectionColumns, tabel1, where, operator, colTabel2, tabel2, db);


//        System.out.println("Salariati---------------------------");
//        afiseazaLista(getAllRecords("ShopSalariati"));
//        System.out.println("se apeleaza hash join");
//        hashJoin("Shop", "-", "Magazin.idMag,Magazin.nume,Angajati.idAng,Angajati.nume", "Angajati.idMagazin=Magazin.idMag", "-", "Angajati.nume");
//        hashJoin("Shop", "-", "*", "Angajati.idMagazin=Magazin.idMag", "-","groupby");
    }

    public int adaugaRecord(String database, String table, List<String> fields) {
        int checkFK = 1;
        myDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
        env = myDbEnv.getEnvironment();
        String dbname = database + table;
        String aData = "";
        String aKey = "";
        //gaseste daca ar fk si verifica daca exista record cu id-ul dat
        List<Integer> fks = new ArrayList<>();
        for (int i = 0; i < fields.size(); i++) {
            if (fields.get(i).contains("Foreign key")) {
                fks.add(i);
            }
        }

        for (int i = 0; i < fks.size(); i++) {
            String bd = database + fields.get(fks.get(i) + 3);
            Database thisDb = myDbEnv.createDatabase(bd, false);
            String key = fields.get(fks.get(i) + 5);
//            System.out.println("-------------------------------");
//            System.out.println("bd: "+ fields.get(fks.get(i)+3) + " key: "+key );
            if (getRecord(bd, key) == "") {
                thisDb.close();
                checkFK = 0;
                break;
            }
            thisDb.close();
        }

        if (checkFK == 1) {
            for (int i = 1; i < fields.size(); i = i + 2) {
                if (fields.get(i - 1).contains("pk")) {
                    if (!aKey.isEmpty()) {
                        aKey += "#";
                        aKey += fields.get(i);
                    } else {
                        aKey += fields.get(i);
                    }

                } else {
                    if (!fields.get(i - 1).contains("Foreign key") && !fields.get(i - 1).contains("References to")) {
                        if (!aData.isEmpty()) {
                            aData += "#";
                            aData += fields.get(i);
                        } else {
                            aData += fields.get(i);
                        }
                    }
                }
            }
            if (getRecord(dbname, aKey) != "") {
                return -1;
            }
            System.out.println("------------------------------MasterFile---------------------------");
            System.out.println("bd:" + dbname);
            try {
                Database newDb = myDbEnv.createDatabase(dbname, false);
                Transaction txn = env.beginTransaction(null, null);
                try {
                    DatabaseEntry theKey = new DatabaseEntry(aKey.getBytes("UTF-8"));
                    DatabaseEntry theData = new DatabaseEntry(aData.getBytes("UTF-8"));
                    newDb.put(txn, theKey, theData);
                    txn.commit();
                    printAllRecords(newDb);
                    System.out.println("-----------------------------------------------------------");
                    System.out.println();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (txn != null) {
                        txn.abort();
                        txn = null;
                    }
                }

            } catch (DatabaseException dbe) {
                // Error code goes here
            } finally {
                myDbEnv.close();

            }
            return 1;
        }
        return 0;
    }

    public String deleteRecord(String database, String table, List<String> values) {
        myDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
        env = myDbEnv.getEnvironment();
        String toReturn = "";
        String id = "";
        for (int i = 0; i < values.size(); i++) {
            if (id == "") {
                id += values.get(i);

            } else {
                id += "#" + values.get(i);
            }

        }
        String dbname = database + table;
        Database newDb = myDbEnv.createDatabase(dbname, false);
        toReturn = getRecord(dbname, id);
        if (toReturn != "") {
            toReturn += "&" + id;
            try {
                Transaction txn = env.beginTransaction(null, null);
                try {
                    System.out.println("BEFORE:");
                    System.out.println("bd:" + dbname);
                    printAllRecords(newDb);
                    System.out.println("-------------------------");
                    DatabaseEntry theKey = new DatabaseEntry(id.getBytes("UTF-8"));
                    newDb.delete(txn, theKey);
                    txn.commit();
                    System.out.println("AFTER:");
                    printAllRecords(newDb);
                    System.out.println("-------------------------");
                } catch (Exception e) {
                    e.printStackTrace();
                    if (txn != null) {
                        txn.abort();
                    }
                }

            } catch (DatabaseException dbe) {
                // Error code goes here
            } finally {
                myDbEnv.close();

            }

        }
        return toReturn;
    }

    public String getRecord(String dbname, String key) {
        String toReturn = "";
        myDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
        env = myDbEnv.getEnvironment();
        Database db = myDbEnv.createDatabase(dbname, false);

        try {
            // Create a pair of DatabaseEntry objects. theKey
            // is used to perform the search. theData is used
            // to store the data returned by the get() operation.
            DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
            DatabaseEntry theData = new DatabaseEntry();

            // Perform the get.
            if (db.get(null, theKey, theData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                // Recreate the data String.
                byte[] retData = theData.getData();
                String foundData = new String(retData, "UTF-8");
//                System.out.println("For key: '" + key + "' found data: '" +
//                        foundData + "'.");
                toReturn = foundData;
            } else {
                System.out.println("No record found for key '" + key + "'.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Exception handling goes here
        }
        return toReturn;
    }

    public String cauta(List<Condition> conditions, IndexFile index, List<String> pks, List<String> selectedfields, List<String> allfields) {
        String numeTabel;
        boolean master = false;
        if (index.getFields().containsAll(pks)) {
            numeTabel = index.getDb() + index.getTable();
            master = true;
        } else {
            numeTabel = index.getName();
        }
        Map<String, Integer> dictionar = new HashMap<>();
        String id = "";
        System.out.println("nume tabel este: " + numeTabel);
//        if(conditions==null){
//            return returneazaRecorduri(getAllRecords(numeTabel), index, selectedfields, allfields, pks);
//        }

        for (Condition c : conditions
                ) {
            if (!id.equals("")) {
                id += "#" + c.getValue();
            } else {
                id += c.getValue();
            }
            if (dictionar.containsKey(c.getEq())) {
                dictionar.put(c.getEq(), dictionar.get(c.getEq()) + 1);
            } else {
                dictionar.put(c.getEq(), 1);
            }
        }
//        for (Map.Entry e : dictionar.entrySet()
//                ) {
//            System.out.println(e.getKey() + " - " + e.getValue());
//
//        }

        System.out.println("ALL");
        afiseazaLista(getAllRecords(numeTabel));
        System.out.println("--------------------------------------");


        if (dictionar.containsKey("=") && dictionar.get("=").equals(conditions.size())) {
            System.out.println("=");
            afiseazaLista(getRecordsE(numeTabel, id));
            return returneazaRecorduri(getRecordsE(numeTabel, id), index, selectedfields, allfields, pks, master);

        }

        if (dictionar.containsKey(">=") && dictionar.get(">=").equals(conditions.size())) {
            System.out.println(">=");
            afiseazaLista(getAllRecordsGE(numeTabel, id));
            return returneazaRecorduri(getAllRecordsGE(numeTabel, id), index, selectedfields, allfields, pks, master);


        }

        if (dictionar.containsKey("<=") && dictionar.get("<=").equals(conditions.size())) {
            System.out.println("<=");
            afiseazaLista(getAllRecordsLE(numeTabel, id));
            return returneazaRecorduri(getAllRecordsLE(numeTabel, id), index, selectedfields, allfields, pks, master);


        }
        if (dictionar.containsKey("!=") && dictionar.get("!=").equals(conditions.size())) {
            System.out.println("!=");
            afiseazaLista(getAllRecordsNE(numeTabel, id));
            return returneazaRecorduri(getAllRecordsNE(numeTabel, id), index, selectedfields, allfields, pks, master);


        }
        if (dictionar.containsKey("<") && dictionar.get("<").equals(conditions.size())) {
            System.out.println("<");
            afiseazaLista(getAllRecordsL(numeTabel, id));
            return returneazaRecorduri(getAllRecordsL(numeTabel, id), index, selectedfields, allfields, pks, master);


        }
        if (dictionar.containsKey(">") && dictionar.get(">").equals(conditions.size())) {
            System.out.println(">");
            afiseazaLista(getAllRecordsG(numeTabel, id));
            return returneazaRecorduri(getAllRecordsG(numeTabel, id), index, selectedfields, allfields, pks, master);

        }
        return "";

    }

    public String returneazaRecorduri(List<Record> records, IndexFile index, List<String> selectedfields, List<String> allfields, List<String> pks, boolean master) {
        String toReturn = "<table class=\"table\">";
        String thead = "<thead><tr>";
        String tbody = "<tbody>";
        for (String s : selectedfields) {
            thead += "<th scope=\"col\">" + s + "</th>";
        }
        thead += "</thead>";
        if (master == false) {
            List<Record> recordFromMaster = new ArrayList<>();
            String dbName = index.getDb() + index.getTable();

            if (index.isUnique.equals("true")) {
                for (Record r : records) {
                    recordFromMaster.add(getRecordsE(dbName, r.getValue()).get(0));
                }
            } else {
                for (Record r : records) {
                    List<String> ids = parseazaLista(r.getValue());
                    for (String id : ids
                            ) {
                        recordFromMaster.add(getRecordsE(dbName, id).get(0));
                    }
                }
            }
            for (Record r : recordFromMaster) {
                String s = r.getValue() + "&" + r.getKey();
                Map<String, String> val = values(s, allfields, pks);
                tbody += "<tr>";
                for (String s1 : selectedfields) {
                    tbody += "<td>" + val.get(s1) + "</td>";
                }
                tbody += "</tr>";

            }
        } else {
            //key e cheie
            for (Record r : records) {
                String s = r.getValue() + "&" + r.getKey();
                Map<String, String> val = values(s, allfields, pks);
                tbody += "<tr>";
                for (String s1 : selectedfields) {
                    tbody += "<td>" + val.get(s1) + "</td>";
                }
                tbody += "</tr>";

            }
            //sunt deja in tabelul maser
        }
        tbody += "</tbody>";
        System.out.println("thead:");
        System.out.println(thead);
        System.out.println("tbody:");
        System.out.println(tbody);
        toReturn += thead + tbody + "</table>";
        return toReturn;
    }

    public static void printAllRecords(Database db) {
        Cursor cursor = null;
        try {
            cursor = db.openCursor(null, null);

            // Cursors need a pair of DatabaseEntry objects to operate. These hold
            // the key and data found at any given position in the database.
            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();

            // To iterate, just call getNext() until the last database record has
            // been read. All cursor operations return an OperationStatus, so just
            // read until we no longer see OperationStatus.SUCCESS
            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                // getData() on the DatabaseEntry objects returns the byte array
                // held by that object. We use this to get a String value. If the
                // DatabaseEntry held a byte array representation of some other
                // data type (such as a complex object) then this operation would
                // look considerably different.
                String keyString = new String(foundKey.getData(), "UTF-8");
                String dataString = new String(foundData.getData(), "UTF-8");
                System.out.println("Key | Data : " + keyString + " | " +
                        dataString + "");
            }
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            // Cursors must be closed.
            cursor.close();
        }
    }

    public void adaugaInIndex(IndexFile index, List<String> fields) {
        myDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
        env = myDbEnv.getEnvironment();
        String aData = "";
        String aKey = "";

        if (index.isUnique().equals("true")) {
            for (int i = 1; i < fields.size(); i = i + 2) {
                String exact = fields.get(i - 1).split(":")[0];
                if (fields.get(i - 1).contains("pk")) {
                    exact = fields.get(i - 1).split(" ")[1].split(":")[0];
                    if (!aData.isEmpty()) {
                        aData += "#";
                        aData += fields.get(i);
                    } else {
                        aData += fields.get(i);
                    }

                }
                for (String s : index.getFields()
                        ) {
                    if (s.equals(exact)) {
                        if (!aKey.equals("")) {
                            aKey += "#";
                            aKey += fields.get(i);
                        } else {
                            aKey = fields.get(i);
                        }
                    }
                }

            }
        } else {
            //non unique index
            for (int i = 1; i < fields.size(); i = i + 2) {
                // cheia primara de adaugat in lista
                if (fields.get(i - 1).contains("pk")) {
                    if (!aData.isEmpty()) {
                        aData += "#";
                        aData += fields.get(i);
                    } else {
                        aData += fields.get(i);
                    }
                } else {
                    if (fields.get(i - 1).contains("Field")) {
                        for (String s : index.getFields()
                                ) {
                            if (fields.get(i - 4).contains(s)) {
                                if (!aKey.equals("")) {
                                    aKey += "#";
                                    aKey += fields.get(i);
                                } else {
                                    aKey = fields.get(i);
                                }
                            }
                        }
                    } else {
                        for (String s : index.getFields()
                                ) {
                            if (fields.get(i - 1).contains(s)) {
                                if (!aKey.equals("")) {
                                    aKey += "#";
                                    aKey += fields.get(i);
                                } else {
                                    aKey = fields.get(i);
                                }
                            }
                        }
                    }

                }
//                if (fields.get(i - 1).contains(index.getField())) {
//                    aKey=fields.get(i);
//                }
            }
            if (getRecord(index.getName(), aKey) != "") {
                List<String> lista = parseazaLista(getRecord(index.getName(), aKey));
                lista.add(aData);
                aData = lista.toString();
                //adauga la lista
            } else {
                List<String> newList = new ArrayList<>();
                newList.add(aData);
                aData = newList.toString();
                //adauga cu o lista cu 1 singur element
            }
        }

        System.out.println("----------------------Index File-----------------------------------");
        System.out.println("INDEX :" + index.getName());
        try {
            Database newDb = myDbEnv.createDatabase(index.getName(), false);
            Transaction txn = env.beginTransaction(null, null);
            try {
                DatabaseEntry theKey = new DatabaseEntry(aKey.getBytes("UTF-8"));
                DatabaseEntry theData = new DatabaseEntry(aData.getBytes("UTF-8"));
                newDb.put(txn, theKey, theData);
                txn.commit();
                printAllRecords(newDb);
                System.out.println("---------------------------------------------------------");
                System.out.println();
            } catch (Exception e) {
                e.printStackTrace();
                if (txn != null) {
                    txn.abort();
                    txn = null;
                }
            }

        } catch (DatabaseException dbe) {
            // Error code goes here
        } finally {
            myDbEnv.close();

        }
    }

    public Map<String, String> values(String rezultatStergere, List<String> fields, List<String> pks) {
        Map<String, String> dictionar = new HashMap<>();
        String[] kv = rezultatStergere.split("&");
        //kv[0] field, kv[1] id
        String[] ids = kv[1].split("#");
        String[] val = kv[0].split("#");
        for (int i = 0; i < pks.size(); i++) {
            dictionar.put(pks.get(i), ids[i]);
        }
        fields.removeAll(pks);
        for (int i = 0; i < fields.size(); i++) {
            dictionar.put(fields.get(i), val[i]);
        }
        return dictionar;
    }

    public void deleteFromIndex(IndexFile index, String rezultatStergere, List<String> fields, List<String> pks) {
        myDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
        env = myDbEnv.getEnvironment();
        fields.removeAll(pks);
        Database newDb = myDbEnv.createDatabase(index.getName(), false);
        String id = "";
        String[] valori = rezultatStergere.split("#");
        Map<String, String> campuri = values(rezultatStergere, fields, pks);
//        for (Map.Entry e:campuri.entrySet()
//             ) {
//            System.out.println(e.getKey()+" - " +e.getValue());
//
//        }

        for (int i = 0; i < index.getFields().size(); i++) {
            if (!id.equals("")) {
                id += "#" + campuri.get(index.getFields().get(i));
            } else {
                id += campuri.get(index.getFields().get(i));
            }
        }

        if (index.isUnique.equals("true")) {
            if (getRecord(index.getName(), id) != "") {
                stergeRecord(index, id);
            }
        } else {
            String rezultat = getRecord(index.getName(), id);
            //e non unique
            List<String> listaRez = parseazaLista(rezultat);
            if (listaRez.size() == 1) {
                stergeRecord(index, id);
                //sterge de tot
            } else {
                listaRez.remove(rezultatStergere.split("&")[1]);
//                        System.out.println("id este" + rezultatStergere.split("&")[1]);
//                        System.out.println(listaRez.toString());

                try {
                    Transaction txn = env.beginTransaction(null, null);
                    try {
                        System.out.println("----------------------Index File-----------------------------------");
                        System.out.println("INDEX :" + index.getName());
                        System.out.println("BEFORE:");
                        printAllRecords(newDb);
                        System.out.println("-------------------------");
                        DatabaseEntry theKey = new DatabaseEntry(id.getBytes("UTF-8"));
                        DatabaseEntry theData = new DatabaseEntry(listaRez.toString().getBytes("UTF-8"));
                        newDb.put(txn, theKey, theData);
                        txn.commit();

                        System.out.println("AFTER:");
                        printAllRecords(newDb);
                        System.out.println("---------------------------------------------------------");
                        System.out.println();
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (txn != null) {
                            txn.abort();
                            txn = null;
                        }
                    }

                } catch (DatabaseException dbe) {
                    // Error code goes here
                } finally {
                    myDbEnv.close();

                }
            }
        }
    }

    public void stergeRecord(IndexFile index, String id) {
        myDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
        env = myDbEnv.getEnvironment();
        Database newDb = myDbEnv.createDatabase(index.getName(), false);
        try {
            Transaction txn = env.beginTransaction(null, null);
            try {
                System.out.println("----------------------Index File-----------------------------------");
                System.out.println("INDEX:" + index.getName());
                System.out.println("BEFORE:");
                printAllRecords(newDb);
                System.out.println("-------------------------");
                DatabaseEntry theKey = new DatabaseEntry(id.getBytes("UTF-8"));
                newDb.delete(txn, theKey);
                txn.commit();
                System.out.println("AFTER:");
                printAllRecords(newDb);
                System.out.println("-------------------------");
            } catch (Exception e) {
                e.printStackTrace();
                if (txn != null) {
                    txn.abort();
                }
            }

        } catch (DatabaseException dbe) {
            // Error code goes here
        } finally {
            myDbEnv.close();

        }

    }

    public static List<String> parseazaLista(String lista) {
        List<String> toReturn = new ArrayList<>();
        String[] valori = lista.split(",");
        if (valori.length == 1) {
            String[] a = valori[0].split("\\[");
            toReturn.add(a[1].split("\\]")[0]);

        } else {
            for (int i = 0; i < valori.length; i++) {
                if (i == 0) {
                    String[] first = valori[i].split("\\[");
                    toReturn.add(first[1]);
                } else {
                    if (i == valori.length - 1) {
                        String[] last = valori[i].split("\\]");
                        String[] last1 = last[0].split(" ");
                        toReturn.add(last1[1]);
                    } else {
                        String[] midle = valori[i].split(" ");
                        toReturn.add(midle[1]);
                    }
                }
            }
        }
        return toReturn;
    }

    public List<Record> getAllRecordsGE(String dbname, String key) {
        List<Record> records = new ArrayList<>();
        Cursor cursor = null;
        myDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
        env = myDbEnv.getEnvironment();
        Database db = myDbEnv.createDatabase(dbname, false);

        try {
            cursor = db.openCursor(null, null);

            // Cursors need a pair of DatabaseEntry objects to operate. These hold
            // the key and data found at any given position in the database.
            DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));

            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();

            // To iterate, just call getNext() until the last database record has
            // been read. All cursor operations return an OperationStatus, so just
            // read until we no longer see OperationStatus.SUCCESS
            OperationStatus retValue = cursor.getSearchKeyRange(theKey, foundData, LockMode.DEFAULT);
            if (retValue == OperationStatus.NOTFOUND) {
                System.out.println(key + "/" + " not matched in database " + db.getDatabaseName());
            } else {
                if (cursor.count() > 0) {
                    while (retValue == OperationStatus.SUCCESS) {
                        String keyString = new String(theKey.getData(), "UTF-8");
                        String dataString = new String(foundData.getData(), "UTF-8");
                        records.add(new Record(keyString, dataString));
                        retValue = cursor.getNext(theKey, foundData, LockMode.DEFAULT);
                    }
                }
            }

        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            // Cursors must be closed.
            cursor.close();
        }
        return records;
    }

    public List<Record> getAllRecords(String dbname) {
        Cursor cursor = null;
        List<Record> records = new ArrayList<>();
        myDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
        env = myDbEnv.getEnvironment();
        Database db = myDbEnv.createDatabase(dbname, false);

        try {
            cursor = db.openCursor(null, null);

            // Cursors need a pair of DatabaseEntry objects to operate. These hold
            // the key and data found at any given position in the database.
            DatabaseEntry foundKey = new DatabaseEntry();
            DatabaseEntry foundData = new DatabaseEntry();

            // To iterate, just call getNext() until the last database record has
            // been read. All cursor operations return an OperationStatus, so just
            // read until we no longer see OperationStatus.SUCCESS
            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                // getData() on the DatabaseEntry objects returns the byte array
                // held by that object. We use this to get a String value. If the
                // DatabaseEntry held a byte array representation of some other
                // data type (such as a complex object) then this operation would
                // look considerably different.
                String keyString = new String(foundKey.getData(), "UTF-8");
                String dataString = new String(foundData.getData(), "UTF-8");
                records.add(new Record(keyString, dataString));

            }
        } catch (DatabaseException de) {
            System.err.println("Error accessing database." + de);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            // Cursors must be closed.
            cursor.close();
        }
        return records;
    }

    public List<Record> getAllRecordsL(String db, String key) {
        return elimina(getAllRecords(db), getAllRecordsGE(db, key));
    }

    public List<Record> getAllRecordsLE(String db, String key) {
        List<Record> l = new ArrayList<>();
        l = elimina(getAllRecords(db), getAllRecordsGE(db, key));
        l.add(getRecordsE(db, key).get(0));
        return l;
    }

    public List<Record> getAllRecordsG(String db, String key) {
        return elimina(getAllRecordsGE(db, key), getRecordsE(db, key));
    }

    public List<Record> getAllRecordsNE(String db, String key) {

        return elimina(getAllRecords(db), getRecordsE(db, key));
    }

    public List<Record> getRecordsE(String dbname, String key) {

        List<Record> records = new ArrayList<>();
        myDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
        env = myDbEnv.getEnvironment();
        Database db = myDbEnv.createDatabase(dbname, false);

        try {
            // Create a pair of DatabaseEntry objects. theKey
            // is used to perform the search. theData is used
            // to store the data returned by the get() operation.
            DatabaseEntry theKey = new DatabaseEntry(key.getBytes("UTF-8"));
            DatabaseEntry theData = new DatabaseEntry();

            // Perform the get.
            if (db.get(null, theKey, theData, LockMode.DEFAULT) ==
                    OperationStatus.SUCCESS) {
                // Recreate the data String.
                byte[] retData = theData.getData();
                String foundData = new String(retData, "UTF-8");
                records.add(new Record(key, foundData));
            } else {
                System.out.println("No record found for key '" + key + "'.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Exception handling goes here
        }
        return records;

    }

    public List<Record> elimina(List<Record> l1, List<Record> l2) {
        int i = 0;
        while (i < l1.size()) {
            for (int j = 0; j < l2.size(); j++) {
                if (l1.get(i).equal(l2.get(j))) {
                    l1.remove(i);
                }

            }
            i++;
        }
        return l1;

    }

    public void afiseazaLista(List<Record> list) {
        for (Record r : list
                ) {
            System.out.println(r.getKey() + ":" + r.getValue());
        }
    }

    public String indexNLJ(String db, String table1, List<String> fieldsTable1, List<String> pksTable1,
                           List<String> fieldsTable2, List<String> pksTable2,
                           IndexFile indexTabel2, String field1, String field2) {
        // ia recordurile din tabelul 2 - care are index
        List<Record> indexRecords = getAllRecords(indexTabel2.getName());
        String tbody = "<tbody>";
        // din field-urile tabelului 2 elmina coloana duplicat
        List<String> fTable2 = new ArrayList<>();
        fTable2.addAll(fieldsTable2);
        fTable2.remove(field2);
        // construieste header-ul tabelului - format din coloanele din ambele tabele
        List<String> tableHeader = new ArrayList<>();
        tableHeader.addAll(fTable2);
        tableHeader.addAll(fieldsTable1);

        // creeaza o copie a field-urilor din tabelul 1
        List<String> copieFieldsTable1 = new ArrayList<>();
        copieFieldsTable1.addAll(fieldsTable1);

        for (Record r : indexRecords
                ) {
            // r.getvalue o sa fie o lista concatenata de id-uri din tabelul 1
            List<String> idTable2 = parseazaLista(r.getValue()); //
            String rezT1 = getRecord(db + table1, r.getKey());
            rezT1 += "&" + r.getKey();
            Map<String, String> dictionarT1 = values(rezT1, copieFieldsTable1, pksTable1);

            for (String id : idTable2
                    ) {
                String rez = getRecord(db + indexTabel2.getTable(), id);
                rez += "&" + id;
                tbody += "<tr>";
                Map<String, String> dictionar = values(rez, fieldsTable2, pksTable2);
                for (String field : fTable2
                        ) {
                    tbody += "<td>" + dictionar.get(field) + "</td>";
                }
                for (String fieldT1 : fieldsTable1
                        ) {
                    tbody += "<td>" + dictionarT1.get(fieldT1) + "</td>";
                }
                tbody += "</tr>";

            }
        }
        String toReturn = "<table class=\"table\">";
        String thead = "<thead><tr>";
        for (String s : tableHeader) {
            thead += "<th scope=\"col\">" + s + "</th>";
        }
        thead += "</tr></thead>";
        toReturn += thead + tbody + "</tbody></table>";
        return toReturn;
    }

    // value imi returneaza dictionar de k-v pentru un string field&id
    //introduc tabela care are fk intr-un

    //hashJoin("Shop","Magazin.idMag > 2","Magazin.idMag,Magazin.nume,Angajati.idAng,Angajati.nume","Magazin.idMag=Angajati.idMagazin")
//    public void hashJoin(String db, String table1, String table2, List<String> fieldsTable1, List<String> fieldsTable2, String having,String projectionColumn,String joinCond1) {
    public String hashJoin(String db, String having, String projectionColumn, String joinCond1, String joinCond2, String groupby) {
        HashMap<Integer, String> hmap = new HashMap<Integer, String>();
        HashMap<Integer, String> hmap2 = new HashMap<Integer, String>();
        List<String> toReturn = new ArrayList<>();
        List<String> toReturn2 = new ArrayList<String>();

        int flag = 0;
        String tabel1JC = joinCond1.split("=")[0].split("\\.")[0];
        String colTabel1JC = joinCond1.split("=")[0].split("\\.")[1];
        String tabel2JC = joinCond1.split("=")[1].split("\\.")[0];
        String colTabel2JC = joinCond1.split("=")[1].split("\\.")[1];
        if (domParser.getPks(db, tabel1JC).contains(colTabel1JC) && domParser.getPks(db, tabel1JC).size() == 1) {
            // acesta este tabelul fara fk
            List<Record> records = getAllRecords(db + tabel1JC);
            Map<String, String> dictionar;
            List<String> pksTabel1JC = domParser.getPks(db, tabel1JC);
            List<String> columnsTabel1JC = domParser.getFields(db, tabel1JC);
            System.out.println("coloane: " + columnsTabel1JC);
            for (Record r : records
                    ) {
                int key = 0;
                dictionar = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel1JC), pksTabel1JC);
                key = Integer.parseInt(dictionar.get(pksTabel1JC.get(0)));
                String value = "";
                for (String field : columnsTabel1JC
                        ) {
                    if (!value.isEmpty()) {
                        value += "#";
                    }
                    value += tabel1JC + "." + field + "=" + dictionar.get(field);
                }
                hmap.put(key, value);
            }
            flag = 1;
        } else {
            //tabel2JC e tabelul fara fk

            if (domParser.getPks(db, tabel2JC).contains(colTabel2JC) && domParser.getPks(db, tabel2JC).size() == 1) {
                // acesta este tabelul fara fk
                List<Record> records = getAllRecords(db + tabel2JC);
                Map<String, String> dictionar;
                List<String> pksTabel2JC = domParser.getPks(db, tabel2JC);
                List<String> columnsTabel2JC = domParser.getFields(db, tabel2JC);
                System.out.println("coloane: " + columnsTabel2JC);
                for (Record r : records
                        ) {
                    int key = 0;
                    dictionar = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel2JC), pksTabel2JC);
                    key = Integer.parseInt(dictionar.get(pksTabel2JC.get(0)));
                    String value = "";
                    for (String field : columnsTabel2JC
                            ) {
                        if (!value.isEmpty()) {
                            value += "#";
                        }
                        value += tabel2JC + "." + field + "=" + dictionar.get(field);
                    }
                    hmap.put(key, value);
                }
                flag = 2;
            }
        }
        if (flag == 1) {
            //parcurge tabelul 2 din JC
            Map<String, String> dictionar;
            List<Record> recordTab2JC = getAllRecords(db + tabel2JC);
            List<String> pksTabel2JC = domParser.getPks(db, tabel2JC);
            List<String> columnsTabel2JC = domParser.getFields(db, tabel2JC);
            columnsTabel2JC.removeAll(pksTabel2JC);
            for (Record r : recordTab2JC
                    ) {
                dictionar = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel2JC), pksTabel2JC);
                String rezultat = "";
                for (Map.Entry e : dictionar.entrySet()
                        ) {
                    rezultat += tabel2JC + "." + e.getKey() + "=" + e.getValue() + "#";
                }
                int fk = Integer.parseInt(dictionar.get(colTabel2JC));
                rezultat += hmap.get(fk);
                toReturn.add(rezultat);
            }
            for (String s : toReturn
                    ) {
                System.out.println(s);
            }
        } else {
            if (flag == 2) {
                //parcurge tabelul 1 din JC
                Map<String, String> dictionar;
                List<Record> recordTab1JC = getAllRecords(db + tabel1JC);
                List<String> pksTabel1JC = domParser.getPks(db, tabel1JC);
                List<String> columnsTabel1JC = domParser.getFields(db, tabel1JC);
                columnsTabel1JC.removeAll(pksTabel1JC);
                for (Record r : recordTab1JC
                        ) {
                    dictionar = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel1JC), pksTabel1JC);
                    String rezultat = "";
                    for (Map.Entry e : dictionar.entrySet()
                            ) {
                        rezultat += tabel1JC + "." + e.getKey() + "=" + e.getValue() + "#";
                    }
                    int fk = Integer.parseInt(dictionar.get(colTabel1JC));
                    rezultat += hmap.get(fk);
                    toReturn.add(rezultat);
                }
                for (String s : toReturn
                        ) {
                    System.out.println(s);
                }
            }
        }
        System.out.println("conditia 2 de join: " + joinCond2);
        //al doilea join
        if (!joinCond2.equals("-")) {
            String tabel1JC2 = joinCond2.split("=")[0].split("\\.")[0];
            String colTabel1JC2 = joinCond2.split("=")[0].split("\\.")[1];
            String tabel2JC2 = joinCond2.split("=")[1].split("\\.")[0];
            String colTabel2JC2 = joinCond2.split("=")[1].split("\\.")[1];
            String join = toReturn.get(0);
            System.out.println(joinCond2.split("=")[0]);
            if (join.contains(joinCond2.split("=")[0])) {
                //creez hash map pentru tabel2
                List<Record> records = getAllRecords(db + tabel2JC2);
                Map<String, String> dictionar;
                List<String> pksTabel2JC2 = domParser.getPks(db, tabel2JC2);
                List<String> columnsTabel2JC2 = domParser.getFields(db, tabel2JC2);
                System.out.println("coloane: " + columnsTabel2JC2);
                if (pksTabel2JC2.contains(colTabel2JC2)) {
                    for (Record r : records
                            ) {
                        int key = 0;
                        dictionar = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel2JC2), pksTabel2JC2);
                        key = Integer.parseInt(dictionar.get(pksTabel2JC2.get(0)));
                        String value = "";
                        for (String field : columnsTabel2JC2
                                ) {
                            if (!value.isEmpty()) {
                                value += "#";
                            }
                            value += tabel2JC2 + "." + field + "=" + dictionar.get(field);
                        }
                        hmap2.put(key, value);
                    }
//                    Set set = hmap2.entrySet();
//                    Iterator iterator = set.iterator();
//                    while (iterator.hasNext()) {
//                        Map.Entry mentry = (Map.Entry) iterator.next();
//                        System.out.print("key is: " + mentry.getKey() + " & Value is: ");
//                        System.out.println(mentry.getValue());
//                    }
                    for (String r : toReturn) {
                        String[] split = r.split("#");
                        int key = 0;
                        for (int i = 0; i < split.length; i++) {
                            if (split[i].contains(joinCond2.split("=")[0])) {
                                key = Integer.parseInt(split[i].split("=")[1]);
                                //splitul resp contine conditia de egalitate
                                //verificam cheile
                                if (hmap2.containsKey(key)) {
                                    toReturn2.add(hmap2.get(key) + "#" + r);
                                }
                            }
                        }
                    }
                    for (String s : toReturn2
                            ) {
                        System.out.println(s);
                    }
                }
            } else {
                if (join.contains(joinCond2.split("=")[1])) {
                    //creez hash map pentru tabel1
                    List<Record> records = getAllRecords(db + tabel1JC2);
                    Map<String, String> dictionar;
                    hmap.clear();
                    List<String> pksTabel1JC2 = domParser.getPks(db, tabel1JC2);
                    List<String> columnsTabel1JC2 = domParser.getFields(db, tabel1JC2);
                    System.out.println("coloane: " + columnsTabel1JC2);
                    if (pksTabel1JC2.contains(colTabel1JC2)) {
                        for (Record r : records
                                ) {
                            int key = 0;
                            dictionar = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel1JC2), pksTabel1JC2);
                            key = Integer.parseInt(dictionar.get(pksTabel1JC2.get(0)));
                            String value = "";
                            for (String field : columnsTabel1JC2
                                    ) {
                                if (!value.isEmpty()) {
                                    value += "#";
                                }
                                value += tabel1JC2 + "." + field + "=" + dictionar.get(field);
                            }
                            hmap2.put(key, value);
                        }
//                        Set set = hmap2.entrySet();
//                        Iterator iterator = set.iterator();
//                        while (iterator.hasNext()) {
//                            Map.Entry mentry = (Map.Entry) iterator.next();
//                            System.out.print("key is: " + mentry.getKey() + " & Value is: ");
//                            System.out.println(mentry.getValue());
//                        }
                        for (String r : toReturn) {
                            String[] split = r.split("#");
                            int key = 0;
                            for (int i = 0; i < split.length; i++) {
                                if (split[i].contains(joinCond2.split("=")[1])) {
                                    key = Integer.parseInt(split[i].split("=")[1]);
                                    //splitul resp contine conditia de egalitate
                                    //verificam cheile
                                    if (hmap2.containsKey(key)) {
                                        toReturn2.add(hmap2.get(key) + "#" + r);
                                    }
                                }
                            }
                        }
                        for (String s : toReturn2
                                ) {
                            System.out.println(s);
                        }
                    }
                }
            }
        }
        String ordered="";
        if (joinCond2.equals("-")) {
            if (!having.equals("-") && !groupby.equals("-"))
                ordered = applyHavingAndGroupBy(toReturn, projectionColumn, having, groupby);
        } else {
            if (!having.equals("-") && !groupby.equals("-"))
                ordered = applyHavingAndGroupBy(toReturn2, projectionColumn, having, groupby);
        }

//        return createHtmlTable(ordered);
        return ordered;
    }

    public String applyHavingAndGroupBy(List<String> joinRez, String projectionColumn, String having, String groupby) {
        String[] havingCond = having.split(" ")[0].split("\\(");
        String agregation = having.split(" ")[0].split("\\(")[0];
        String column = having.split(" ")[0].split("\\(")[1].split("\\)")[0];
        String cond = having.split(" ")[1];
        String limitValue = having.split(" ")[2];
        String toReturn = "";
        if (agregation.equals("COUNT")) {
            Map<String, Integer> rez = new HashMap<>();
            for (String s : joinRez
                    ) {
                boolean check = false;
                String gr = "";
                String[] line = s.split("#");
                for (int i = 0; i < line.length; i++) {
                    if (line[i].contains(column)) {
                        check = true;
                    }
                    if (line[i].contains(groupby)) {
                        gr = line[i].split("=")[1];
                    }
                }
                if (check == true && !gr.equals("")) {
                    if (rez.containsKey(gr)) {
                        rez.put(gr, rez.get(gr) + 1);
                    } else {
                        rez.put(gr, 1);
                    }
                }
            }
            toReturn = "<table class=\"table\"><thead><tr>";
            toReturn += "<th scope=\"col\">" + groupby + "</th>";
            toReturn += "<th scope=\"col\">" + having.split(" ")[0] + "</th>";
            toReturn += "</tr></thead><tbody>";
            Set set = rez.entrySet();
            Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                Map.Entry mentry = (Map.Entry) iterator.next();
                String s = mentry.getValue().toString();
                if (checkCondition(cond, Integer.parseInt(s), Integer.parseInt(limitValue)))
                    toReturn += "<tr>" + "<td>" + mentry.getKey() + "</td>" + "<td>" + mentry.getValue() + "</td></tr>";
            }
            toReturn += "</tbody></table>";
        } else {
            Map<String, List<Integer>> rez1 = new HashMap<>();
            for (String s : joinRez
                    ) {
                boolean check = false;
                int val = -1;
                String gr = "";
                String[] line = s.split("#");
                for (int i = 0; i < line.length; i++) {
                    if (line[i].contains(column)) {
                        val = Integer.parseInt(line[i].split("=")[1]);
                        check = true;
                    }
                    if (line[i].contains(groupby)) {
                        gr = line[i].split("=")[1];
                    }
                }
                if (check == true && !gr.equals("")) {
                    if (rez1.containsKey(gr)) {
                        List<Integer> l = rez1.get(gr);
                        l.add(val);
                        rez1.put(gr, l);
                    } else {
                        List<Integer> l = new ArrayList<>();
                        l.add(val);
                        rez1.put(gr, l);
                    }
                }
            }
            toReturn = "<table class=\"table\"><thead><tr>";
            toReturn += "<th scope=\"col\">" + groupby + "</th>";
            toReturn += "<th scope=\"col\">" + having.split(" ")[0] + "</th>";
            toReturn += "</tr></thead><tbody>";
            if (agregation.equals("MIN")) {
                Set set = rez1.entrySet();
                Iterator iterator = set.iterator();
                while (iterator.hasNext()) {
                    Map.Entry mentry = (Map.Entry) iterator.next();
                    List<Integer> l = rez1.get(mentry.getKey().toString());
                    int min = getMin(l);
                    if (checkCondition(cond, min, Integer.parseInt(limitValue)))
                        toReturn += "<tr>" + "<td>" + mentry.getKey() + "</td>" + "<td>" + min + "</td></tr>";
                }
            }
            if (agregation.equals("MAX")) {
                Set set = rez1.entrySet();
                Iterator iterator = set.iterator();
                while (iterator.hasNext()) {
                    Map.Entry mentry = (Map.Entry) iterator.next();
                    List<Integer> l = rez1.get(mentry.getKey().toString());
                    int max = getMax(l);
                    if (checkCondition(cond, max, Integer.parseInt(limitValue)))
                        toReturn += "<tr>" + "<td>" + mentry.getKey() + "</td>" + "<td>" + max + "</td></tr>";
                }
            }
            if (agregation.equals("SUM")) {
                Set set = rez1.entrySet();
                Iterator iterator = set.iterator();
                while (iterator.hasNext()) {
                    Map.Entry mentry = (Map.Entry) iterator.next();
                    List<Integer> l = rez1.get(mentry.getKey().toString());
                    int sum = getSum(l);
                    if (checkCondition(cond, sum, Integer.parseInt(limitValue)))
                        toReturn += "<tr>" + "<td>" + mentry.getKey() + "</td>" + "<td>" + sum + "</td></tr>";
                }
            }
            if (agregation.equals("AVG")) {
                Set set = rez1.entrySet();
                Iterator iterator = set.iterator();
                while (iterator.hasNext()) {
                    Map.Entry mentry = (Map.Entry) iterator.next();
                    List<Integer> l = rez1.get(mentry.getKey().toString());
                    double avg = getAvg(l);
                    if (checkConditionForDouble(cond, avg, Double.parseDouble(limitValue)))
                        toReturn += "<tr>" + "<td>" + mentry.getKey() + "</td>" + "<td>" + avg + "</td></tr>";
                }
            }
            toReturn += "</tbody></table>";

            Set set = rez1.entrySet();
            Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                Map.Entry mentry = (Map.Entry) iterator.next();
                System.out.print("key is: " + mentry.getKey() + " & Value is: ");
                System.out.println(mentry.getValue());
            }
        }
        return toReturn;
    }

    public int getMin(List<Integer> list) {
        int toReturn = 999;
        for (Integer r : list
                ) {
            if (r < toReturn)
                toReturn = r;
        }
        return toReturn;
    }

    public int getMax(List<Integer> list) {
        int toReturn = 0;
        for (Integer r : list
                ) {
            if (r > toReturn)
                toReturn = r;
        }
        return toReturn;
    }

    public int getSum(List<Integer> list) {
        int toReturn = 0;
        for (Integer r : list
                ) {
            toReturn += r;
        }
        return toReturn;
    }

    public double getAvg(List<Integer> list) {
        double toReturn = 0;
        for (Integer r : list
                ) {
            toReturn += r;
        }
        return toReturn / list.size();
    }


    public List<Record> applyProjection(List<String> joinRez, String projectionColumn, String having, String groupby) {
        //parseaza projection columns
        List<String> projectionCol = new ArrayList<>();
        if (!projectionColumn.equals("*")) {
            String[] projCol = projectionColumn.split(",");
            for (int i = 0; i < projCol.length; i++) {
                projectionCol.add(projCol[i]);
            }
        }
        System.out.println("HAVING: " + having);
        List<Record> sortedList = new ArrayList<>();
        List<Record> nogroupby = new ArrayList<>();
        List<String> toReturn = new ArrayList<>();
        for (String s : joinRez
                ) {
            String[] split = s.split("#");
            String rezLinie = "";
            String gb = "";
            boolean check = false;
            for (int i = 0; i < split.length; i++) {
                String col = split[i].split("=")[0];
                String value = split[i].split("=")[1];
                if (projectionCol.contains(col) || projectionColumn.equals("*")) {
                    if (!rezLinie.isEmpty()) {
                        rezLinie += "#";
                    }
                    rezLinie += split[i];
                    if (checkHaving(col, value, having)) {
                        check = true;
                    }
                    if (groupby.equals(col)) {
                        gb = value;
                    }
                }

            }
            if (check == true) {
                toReturn.add(rezLinie);
            }
            if (check == true && !gb.equals("")) {
                sortedList.add(new Record(gb, rezLinie));
            } else {
                gb = "index";
                nogroupby.add(new Record(gb, rezLinie));
                //groupby
            }
        }
        System.out.println("dupa proiecte si verificare conditie having");
        System.out.println("toReturn");
        for (String s : toReturn
                ) {
            System.out.println(s);
        }
        System.out.println("sortedlist:");
        if (sortedList.size() > 0) {
            for (Record s : sortedList
                    ) {
                System.out.println(s.getKey() + "-------" + s.getValue());
            }
        } else {
            for (Record s : nogroupby
                    ) {
                System.out.println(s.getKey() + "-------" + s.getValue());
            }
        }
        System.out.println("AFTER");
//        sortedList.sort(String.CASE_INSENSITIVE_ORDER);
        if (sortedList.size() > 0) {
            sortedList.sort(Comparator.comparing(Record::getKey));
            for (Record s : sortedList
                    ) {
                System.out.println(s.getKey() + "-----" + s.getValue());
            }
            return sortedList;
        }
        return nogroupby;
    }

    public String leftJoin(String projectionColumns, String db, String tabel1, String tabel2, String joinCondition) {
//        List<IndexFile> indecsiTabel1=domParser.getIndex(db+tabel1,tabel1);
        String[] columnsProjection = projectionColumns.split(",");
        List<String> coloaneSelect = new ArrayList<>();
        for (int i = 0; i < columnsProjection.length; i++) {
            coloaneSelect.add(columnsProjection[i]);
        }
        String jcFirstTable = joinCondition.split(" ")[0].split("\\.")[0];
        String tabel1JC = "";
        String tabel2JC = "";
        String colTabel1JC = "";
        String colTabel2JC = "";
        if (jcFirstTable.equals(tabel1)) {
            //mergi mai departe cu split
            tabel1JC = joinCondition.split(" ")[0].split("\\.")[0];
            colTabel1JC = joinCondition.split(" ")[0].split("\\.")[1];
            tabel2JC = joinCondition.split(" ")[2].split("\\.")[0];
            colTabel2JC = joinCondition.split(" ")[2].split("\\.")[1];
        } else {
            //switch
            tabel1JC = joinCondition.split(" ")[2].split("\\.")[0];
            colTabel1JC = joinCondition.split(" ")[2].split("\\.")[1];
            tabel2JC = joinCondition.split(" ")[0].split("\\.")[0];
            colTabel2JC = joinCondition.split(" ")[0].split("\\.")[1];
        }
        System.out.println("tabel 1:" + tabel1JC);
        System.out.println("tabel 2:" + tabel2JC);
        System.out.println("coloana 1:" + colTabel1JC);
        System.out.println("coloana 2:" + colTabel2JC);
        if (domParser.getFKs(db, tabel1).size() > 0) {
            //arunca eroare
        } else {
            List<IndexFile> indecsiTabel2 = domParser.getIndex(db, tabel2);
            List<String> columns = new ArrayList<>();
            columns.add(colTabel2JC);
            IndexFile indexFK = getSpecificIndex(indecsiTabel2, columns);
            List<Record> recordsTabel1 = getAllRecords(db + tabel1);

            if (indexFK != null) {
                return createHtmlContent(leftJoinUsingIndex(recordsTabel1, coloaneSelect, db, tabel1JC, indexFK, tabel2JC));
            } else {
                return createHtmlContent(leftJoinUsingNoIndex(coloaneSelect, db, tabel1JC, tabel2JC, colTabel1JC, colTabel2JC));
            }

        }
        return "";
    }

    public List<String> leftJoinUsingIndex(List<Record> recordsTabel1, List<String> coloaneSelect, String db, String tabel1JC, IndexFile indexFK, String tabel2JC) {
        List<String> rezultat = new ArrayList<>();
        for (Record r : recordsTabel1
                ) {
            String rez = "";
            Map<String, String> dictionar = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel1JC), domParser.getPks(db, tabel1JC));
            for (String s : coloaneSelect
                    ) {
                String tabel = s.split("\\.")[0];
                String coloana = s.split("\\.")[1];
                if (tabel.equals(tabel1JC)) {
                    String val = dictionar.get(coloana);
                    rez += s + "=" + val + "#";
                }
            }
            String getR = getRecord(indexFK.getName(), r.getKey());
            //daca nu exista parcuge rand cu rand din tabelul 2 si returneaza lista cu recorduri in care apare cheia respectiva
            if (!getR.equals("")) {
                List<String> ids = parseazaLista(getR);
                for (String id : ids
                        ) {
                    String rez2 = "";
                    String rezCautareTabel2 = getRecord(db + tabel2JC, id);
                    Map<String, String> dictionar2 = values(rezCautareTabel2 + "&" + id, domParser.getFields(db, tabel2JC), domParser.getPks(db, tabel2JC));

                    for (String s : coloaneSelect
                            ) {
                        String tabel = s.split("\\.")[0];
                        String coloana = s.split("\\.")[1];
                        if (tabel.equals(tabel2JC)) {
                            String val = dictionar2.get(coloana);
                            rez2 += s + "=" + val + "#";
                        }
                    }
                    rezultat.add(rez + rez2);
                }
                //adauga in rezultat toate aparitiile
            } else {
                String rez2 = "";
                for (String s : coloaneSelect
                        ) {
                    String tabel = s.split("\\.")[0];
                    String coloana = s.split("\\.")[1];
                    if (tabel.equals(tabel2JC)) {
                        rez2 += s + "=" + "NULL#";
                    }
                    //adauga null
                }
                rezultat.add(rez + rez2);
            }

        }
        for (String s : rezultat
                ) {
            System.out.println(s);

        }
        return rezultat;
    }

    public List<String> leftJoinUsingNoIndex(List<String> coloaneSelect, String db, String tabel1JC, String tabel2JC, String colTabel1JC, String colTabel2JC) {
        System.out.println("leftJoinUsingNoIndex");
        List<String> rezultat = new ArrayList<>();
        List<Record> recordsTabel1 = new ArrayList<>();
        recordsTabel1 = getAllRecords(db + tabel1JC);
        List<Record> recordsTabel2 = new ArrayList<>();
        recordsTabel2 = getAllRecords(db + tabel2JC);
        for (Record r : recordsTabel1
                ) {
            String rez = "";
            Map<String, String> dictionar = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel1JC), domParser.getPks(db, tabel1JC));
            for (String s : coloaneSelect
                    ) {
                String tabel = s.split("\\.")[0];
                String coloana = s.split("\\.")[1];
                if (tabel.equals(tabel1JC)) {
                    String val = dictionar.get(coloana);
                    rez += s + "=" + val + "#";
                }
            }
            boolean gasit = false;
            for (Record r2 : recordsTabel2
                    ) {
                Map<String, String> dictionar2 = values(r2.getValue() + "&" + r2.getKey(), domParser.getFields(db, tabel2JC), domParser.getPks(db, tabel2JC));

                //check de join cond
                if (dictionar.get(colTabel1JC).equals(dictionar2.get(colTabel2JC))) {
                    String rez2 = "";
                    for (String s : coloaneSelect
                            ) {
                        String tabel = s.split("\\.")[0];
                        String coloana = s.split("\\.")[1];
                        if (tabel.equals(tabel2JC)) {
                            String val = dictionar2.get(coloana);
                            rez2 += s + "=" + val + "#";
                        }
                    }
                    gasit = true;
                    rezultat.add(rez + rez2);

                }
            }
            if (gasit == false) {
                //adauga null
                String rez2 = "";
                for (String s : coloaneSelect
                        ) {
                    String tabel = s.split("\\.")[0];
                    String coloana = s.split("\\.")[1];
                    if (tabel.equals(tabel2JC)) {
                        rez2 += s + "=" + "NULL#";
                    }
                }
                rezultat.add(rez + rez2);
            }

        }

        for (String s : rezultat) {
            System.out.println(s);
        }
        return rezultat;

    }

    public String rightJoin(String projectionColumns, String db, String tabel1, String tabel2, String joinCondition) {
        String[] columnsProjection = projectionColumns.split(",");
        List<String> coloaneSelect = new ArrayList<>();
        for (int i = 0; i < columnsProjection.length; i++) {
            coloaneSelect.add(columnsProjection[i]);
        }
        String jcFirstTable = joinCondition.split(" ")[0].split("\\.")[0];
        String tabel1JC = "";
        String tabel2JC = "";
        String colTabel1JC = "";
        String colTabel2JC = "";
        if (jcFirstTable.equals(tabel1)) {
            //mergi mai departe cu split
            tabel1JC = joinCondition.split(" ")[0].split("\\.")[0];
            colTabel1JC = joinCondition.split(" ")[0].split("\\.")[1];
            tabel2JC = joinCondition.split(" ")[2].split("\\.")[0];
            colTabel2JC = joinCondition.split(" ")[2].split("\\.")[1];
        } else {
            //switch
            tabel1JC = joinCondition.split(" ")[2].split("\\.")[0];
            colTabel1JC = joinCondition.split(" ")[2].split("\\.")[1];
            tabel2JC = joinCondition.split(" ")[0].split("\\.")[0];
            colTabel2JC = joinCondition.split(" ")[0].split("\\.")[1];
        }
        System.out.println("tabel 1:" + tabel1JC);
        System.out.println("tabel 2:" + tabel2JC);
        System.out.println("coloana 1:" + colTabel1JC);
        System.out.println("coloana 2:" + colTabel2JC);
        return createHtmlContent(rightJoinUsingNoIndex(coloaneSelect, db, tabel1JC, tabel2JC, colTabel1JC, colTabel2JC));

    }

    public String fullJoin(String projectionColumns, String db, String tabel1, String tabel2, String joinCondition) {
        String[] columnsProjection = projectionColumns.split(",");
        List<String> coloaneSelect = new ArrayList<>();
        for (int i = 0; i < columnsProjection.length; i++) {
            coloaneSelect.add(columnsProjection[i]);
        }
        String jcFirstTable = joinCondition.split(" ")[0].split("\\.")[0];
        String tabel1JC = "";
        String tabel2JC = "";
        String colTabel1JC = "";
        String colTabel2JC = "";
        if (jcFirstTable.equals(tabel1)) {
            //mergi mai departe cu split
            tabel1JC = joinCondition.split(" ")[0].split("\\.")[0];
            colTabel1JC = joinCondition.split(" ")[0].split("\\.")[1];
            tabel2JC = joinCondition.split(" ")[2].split("\\.")[0];
            colTabel2JC = joinCondition.split(" ")[2].split("\\.")[1];
        } else {
            //switch
            tabel1JC = joinCondition.split(" ")[2].split("\\.")[0];
            colTabel1JC = joinCondition.split(" ")[2].split("\\.")[1];
            tabel2JC = joinCondition.split(" ")[0].split("\\.")[0];
            colTabel2JC = joinCondition.split(" ")[0].split("\\.")[1];
        }
        System.out.println("tabel 1:" + tabel1JC);
        System.out.println("tabel 2:" + tabel2JC);
        System.out.println("coloana 1:" + colTabel1JC);
        System.out.println("coloana 2:" + colTabel2JC);
        List<String> rezultat = new ArrayList<>();
        rezultat.addAll(leftJoinUsingNoIndex(coloaneSelect, db, tabel1JC, tabel2JC, colTabel1JC, colTabel2JC));
        rezultat.addAll(helperFullJoin(rightJoinUsingNoIndex(coloaneSelect, db, tabel1JC, tabel2JC, colTabel1JC, colTabel2JC)));

        //        System.out.println("REZULTAT FINAL");
//        for (String s : rezultat
//                ) {
//            System.out.println(s);
//
//        }

        Set<String> set = new HashSet<>(rezultat);
        rezultat.clear();
        rezultat.addAll(set);
        rezultat.sort(String.CASE_INSENSITIVE_ORDER);

//        System.out.println("REZULTAT FARA DUPLICATE");
//        for (String s : rezultat
//                ) {
//            System.out.println(s);
//
//        }

        return createHtmlContent(rezultat);
    }

    public List<String> helperFullJoin(List<String> list) {
        List<String> rez = new ArrayList<>();
        for (String s : list
                ) {
            String r = s.split("#")[2] + "#" + s.split("#")[3] + "#" + s.split("#")[0] + "#" + s.split("#")[1] + "#";
            rez.add(r);
        }
        return rez;
    }

    public List<String> rightJoinUsingNoIndex(List<String> coloaneSelect, String db, String tabel1JC, String tabel2JC, String colTabel1JC, String colTabel2JC) {
        System.out.println("rightJoinUsingNoIndex");
        List<String> rezultat = new ArrayList<>();
        List<Record> recordsTabel1 = new ArrayList<>();
        recordsTabel1 = getAllRecords(db + tabel1JC);
        List<Record> recordsTabel2 = new ArrayList<>();
        recordsTabel2 = getAllRecords(db + tabel2JC);
        for (Record r : recordsTabel2
                ) {
            String rez = "";
            Map<String, String> dictionar = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel2JC), domParser.getPks(db, tabel2JC));
            for (String s : coloaneSelect
                    ) {
                String tabel = s.split("\\.")[0];
                String coloana = s.split("\\.")[1];
                if (tabel.equals(tabel2JC)) {
                    String val = dictionar.get(coloana);
                    rez += s + "=" + val + "#";
                }
            }
            boolean gasit = false;
            for (Record r2 : recordsTabel1
                    ) {
                Map<String, String> dictionar2 = values(r2.getValue() + "&" + r2.getKey(), domParser.getFields(db, tabel1JC), domParser.getPks(db, tabel1JC));

                //check de join cond
                if (dictionar.get(colTabel1JC).equals(dictionar2.get(colTabel2JC))) {
                    String rez2 = "";
                    for (String s : coloaneSelect
                            ) {
                        String tabel = s.split("\\.")[0];
                        String coloana = s.split("\\.")[1];
                        if (tabel.equals(tabel1JC)) {
                            String val = dictionar2.get(coloana);
                            rez2 += s + "=" + val + "#";
                        }
                    }
                    gasit = true;
                    rezultat.add(rez + rez2);

                }
            }
            if (gasit == false) {
                //adauga null
                String rez2 = "";
                for (String s : coloaneSelect
                        ) {
                    String tabel = s.split("\\.")[0];
                    String coloana = s.split("\\.")[1];
                    if (tabel.equals(tabel1JC)) {
                        rez2 += s + "=" + "NULL#";
                    }
                }
                rezultat.add(rez + rez2);
            }

        }

        for (String s : rezultat) {
            System.out.println(s);
        }
        return rezultat;
    }

    public String semiJoin(List<String> projectionColumns, String tabel1, String where, String operator, String colTabel2, String tabel2, String db) {
        List<IndexFile> indecsiExistentiTabela2 = domParser.getIndex(db, tabel2);
        List<String> columns = new ArrayList<>();
        columns.add(colTabel2.split("\\.")[1]);
        IndexFile indexTabela2 = getSpecificIndex(indecsiExistentiTabela2, columns);
        List<Record> recordsTabela1 = getAllRecords(db + tabel1);
        List<String> toReturn = new ArrayList<>();
        if (indexTabela2 != null) {
            for (Record r : recordsTabela1
                    ) {
                if (!getRecord(indexTabela2.getName(), r.getKey()).equals("")) {
                    //adauga la rezultat
                    Map<String, String> dictionar1 = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel1), domParser.getPks(db, tabel1));

                    String rez = "";
                    for (String s : projectionColumns
                            ) {
                        String coloana = s.split("\\.")[1];
                        rez += s + "=" + dictionar1.get(coloana) + "#";
                    }
                    toReturn.add(rez);
                }
            }
        } else {
            List<Record> recordsTabela2 = getAllRecords(db + tabel2);
            for (Record r1 : recordsTabela1
                    ) {
                boolean gasit = false;
                for (Record r2 : recordsTabela2) {
                    Map<String, String> dictionar2 = values(r2.getValue() + "&" + r2.getKey(), domParser.getFields(db, tabel2), domParser.getPks(db, tabel2));
                    if (r1.getKey().equals(dictionar2.get(colTabel2.split("\\.")[1]))) {
                        gasit = true;
                        break;
                    }
                }
                if (gasit == true) {
                    Map<String, String> dictionar1 = values(r1.getValue() + "&" + r1.getKey(), domParser.getFields(db, tabel1), domParser.getPks(db, tabel1));

                    String rez = "";
                    for (String s : projectionColumns
                            ) {
                        String coloana = s.split("\\.")[1];
                        rez += s + "=" + dictionar1.get(coloana) + "#";
                    }
                    toReturn.add(rez);
                }
            }
        }
        for (String s : toReturn
                ) {
            System.out.println(s);
        }
        return createHtmlContent(toReturn);
    }

    public String antiJoin(List<String> projectionColumns, String tabel1, String where, String operator, String colTabel2, String tabel2, String db) {
        List<IndexFile> indecsiExistentiTabela2 = domParser.getIndex(db, tabel2);
        List<String> columns = new ArrayList<>();
        columns.add(colTabel2.split("\\.")[1]);
        IndexFile indexTabela2 = getSpecificIndex(indecsiExistentiTabela2, columns);
        List<Record> recordsTabela1 = getAllRecords(db + tabel1);
        List<String> toReturn = new ArrayList<>();
        if (indexTabela2 != null) {
            for (Record r : recordsTabela1
                    ) {
                if (getRecord(indexTabela2.getName(), r.getKey()).equals("")) {
                    //adauga la rezultat
                    Map<String, String> dictionar1 = values(r.getValue() + "&" + r.getKey(), domParser.getFields(db, tabel1), domParser.getPks(db, tabel1));

                    String rez = "";
                    for (String s : projectionColumns
                            ) {
                        String coloana = s.split("\\.")[1];
                        rez += s + "=" + dictionar1.get(coloana) + "#";
                    }
                    toReturn.add(rez);
                }
            }
        } else {
            List<Record> recordsTabela2 = getAllRecords(db + tabel2);
            for (Record r1 : recordsTabela1
                    ) {
                boolean gasit = false;
                for (Record r2 : recordsTabela2) {
                    Map<String, String> dictionar2 = values(r2.getValue() + "&" + r2.getKey(), domParser.getFields(db, tabel2), domParser.getPks(db, tabel2));
                    if (r1.getKey().equals(dictionar2.get(colTabel2.split("\\.")[1]))) {
                        gasit = true;
                        break;
                    }
                }
                if (gasit == false) {
                    Map<String, String> dictionar1 = values(r1.getValue() + "&" + r1.getKey(), domParser.getFields(db, tabel1), domParser.getPks(db, tabel1));

                    String rez = "";
                    for (String s : projectionColumns
                            ) {
                        String coloana = s.split("\\.")[1];
                        rez += s + "=" + dictionar1.get(coloana) + "#";
                    }
                    toReturn.add(rez);
                }
            }
        }
        for (String s : toReturn
                ) {
            System.out.println(s);
        }
        return createHtmlContent(toReturn);
    }


    public IndexFile getSpecificIndex(List<IndexFile> indecsi, List<String> columns) {

        for (IndexFile i : indecsi
                ) {
            if (i.getFields().containsAll(columns)) {
                return i;
            }

        }
        return null;
    }

    public boolean checkCondition(String cond, int actualValue, int limitValue) {
        if (cond.equals(">")) {
            if (actualValue > limitValue)
                return true;
        }
        if (cond.equals("=")) {
            if (actualValue == limitValue)
                return true;
        }
        if (cond.equals("<")) {
            if (actualValue < limitValue)
                return true;
        }

        if (cond.equals("<=")) {
            if (actualValue <= limitValue)
                return true;
        }
        if (cond.equals(">=")) {
            if (actualValue >= limitValue)
                return true;
        }
        return false;
    }

    public boolean checkConditionForDouble(String cond, double actualValue, double limitValue) {
        if (cond.equals(">")) {
            if (actualValue > limitValue)
                return true;
        }
        if (cond.equals("=")) {
            if (actualValue == limitValue)
                return true;
        }
        if (cond.equals("<")) {
            if (actualValue < limitValue)
                return true;
        }

        if (cond.equals("<=")) {
            if (actualValue >= limitValue)
                return true;
        }
        if (cond.equals(">=")) {
            if (actualValue >= limitValue)
                return true;
        }
        return false;
    }

    public boolean checkHaving(String column, String val, String having) {
        if (having.equals("-")) {
            return true;
        }
        String[] havingParsed = having.split(" ");
        String col = havingParsed[0];
        String condHaving = havingParsed[1];
        String limitValueHaving = havingParsed[2];
        if (column.equals(col)) {
            if (condHaving.equals(">")) {
                if (val.compareTo(limitValueHaving) > 0)
                    return true;
            }
            if (condHaving.equals("=")) {
                if (val.compareTo(limitValueHaving) == 0)
                    return true;
            }
            if (condHaving.equals("<")) {
                if (val.compareTo(limitValueHaving) < 0)
                    return true;
            }
        }
        return false;
    }

    public String createHtmlTable(List<Record> list) {
        String toReturn = "<table class=\"table\"><thead><tr>";
        String[] split = list.get(0).getValue().split("#");
        for (int i = 0; i < split.length; i++) {
            toReturn += "<th scope=\"col\">" + split[i].split("=")[0] + "</th>";
        }
        toReturn += "</tr></thead><tbody>";
        for (Record r : list
                ) {
            split = r.getValue().split("#");
            toReturn += "<tr>";
            for (int i = 0; i < split.length; i++) {
                toReturn += "<td>" + split[i].split("=")[1] + "</td>";
            }
            toReturn += "</tr>";
        }
        toReturn += "</tbody></table>";
        System.out.println(toReturn);
        return toReturn;
    }

    public String createHtmlContent(List<String> list) {
        String toReturn = "<table class=\"table\"><thead><tr>";
        String[] split = list.get(0).split("#");
        for (int i = 0; i < split.length; i++) {
            toReturn += "<th scope=\"col\">" + split[i].split("=")[0] + "</th>";
        }
        toReturn += "</tr></thead><tbody>";
        for (String s : list
                ) {
            split = s.split("#");
            toReturn += "<tr>";
            for (int i = 0; i < split.length; i++) {
                toReturn += "<td>" + split[i].split("=")[1] + "</td>";
            }
            toReturn += "</tr>";
        }
        toReturn += "</tbody></table>";
        System.out.println(toReturn);
        return toReturn;
    }


}


