//package com.example.isgbd.KeyValueSystem;
//
//import com.sleepycat.je.*;
//
//import java.io.File;
//import java.io.UnsupportedEncodingException;
//
//public class Main {
//    public static void main(String[] argv){
//        MyDbEnv exampleDbEnv = new MyDbEnv();
//        try {
//            exampleDbEnv.setup(new File("C:\\Users\\Irina\\Desktop\\bitbucket\\isgbd\\isgbd\\src\\main\\resources\\dbs"), false);
//            Environment env=exampleDbEnv.getEnvironment();
//            Database newDb=exampleDbEnv.createDatabase("newDb",false);
//            String aKey = "aaaa";
//            String aData = "aaaaaaaaaaafffaa";
//
//            Transaction txn = env.beginTransaction(null, null);
//           try {
//               DatabaseEntry theKey = new DatabaseEntry(aKey.getBytes("UTF-8"));
//               DatabaseEntry theData = new DatabaseEntry(aData.getBytes("UTF-8"));
//               newDb.put(txn, theKey, theData);
//               txn.commit();
//           }catch (Exception e){
//               e.printStackTrace();
//               if (txn != null) {
//                   txn.abort();
//                   txn = null;
//               }
//           }
////            getRecord(newDb);
//            getAllRecords(newDb);
//
//        } catch(DatabaseException dbe) {
//            // Error code goes here
//        } finally {
//            exampleDbEnv.close();
//
//        }
//
//    }
//
//    public static void getRecord(Database db){
//        String aKey = "mysecondkey";
//        System.out.println("intra");
//        try {
//            // Create a pair of DatabaseEntry objects. theKey
//            // is used to perform the search. theData is used
//            // to store the data returned by the get() operation.
//            DatabaseEntry theKey = new DatabaseEntry(aKey.getBytes("UTF-8"));
//            DatabaseEntry theData = new DatabaseEntry();
//
//            // Perform the get.
//            if (db.get(null, theKey, theData, LockMode.DEFAULT) ==
//                    OperationStatus.SUCCESS) {
//
//                // Recreate the data String.
//                byte[] retData = theData.getData();
//                String foundData = new String(retData, "UTF-8");
//                System.out.println("For key: '" + aKey + "' found data: '" +
//                        foundData + "'.");
//            } else {
//                System.out.println("No record found for key '" + aKey + "'.");
//            }
//        } catch (Exception e) {
//            // Exception handling goes here
//        }
//
//        String a1Key = "asasa";
//        try {
//            // Create a pair of DatabaseEntry objects. theKey
//            // is used to perform the search. theData is used
//            // to store the data returned by the get() operation.
//            DatabaseEntry theKey = new DatabaseEntry(a1Key.getBytes("UTF-8"));
//            DatabaseEntry theData = new DatabaseEntry();
//
//            // Perform the get.
//            if (db.get(null, theKey, theData, LockMode.DEFAULT) ==
//                    OperationStatus.SUCCESS) {
//
//                // Recreate the data String.
//                byte[] retData = theData.getData();
//                String foundData = new String(retData, "UTF-8");
//                System.out.println("For key: '" + a1Key + "' found data: '" +
//                        foundData + "'.");
//            } else {
//                System.out.println("No record found for key '" + a1Key + "'.");
//            }
//        } catch (Exception e) {
//            // Exception handling goes here
//        }
//    }
//
//    public static void getAllRecords(Database db){
//        Cursor cursor = null;
//        try {
//            cursor = db.openCursor(null, null);
//
//            // Cursors need a pair of DatabaseEntry objects to operate. These hold
//            // the key and data found at any given position in the database.
//            DatabaseEntry foundKey = new DatabaseEntry();
//            DatabaseEntry foundData = new DatabaseEntry();
//
//            // To iterate, just call getNext() until the last database record has
//            // been read. All cursor operations return an OperationStatus, so just
//            // read until we no longer see OperationStatus.SUCCESS
//            while (cursor.getNext(foundKey, foundData, LockMode.DEFAULT) ==
//                    OperationStatus.SUCCESS) {
//                // getData() on the DatabaseEntry objects returns the byte array
//                // held by that object. We use this to get a String value. If the
//                // DatabaseEntry held a byte array representation of some other
//                // data type (such as a complex object) then this operation would
//                // look considerably different.
//                String keyString = new String(foundKey.getData(), "UTF-8");
//                String dataString = new String(foundData.getData(), "UTF-8");
//                System.out.println("Key | Data : " + keyString + " | " +
//                        dataString + "");
//            }
//        } catch (DatabaseException de) {
//            System.err.println("Error accessing database." + de);
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } finally {
//            // Cursors must be closed.
//            cursor.close();
//        }
//    }
//}
