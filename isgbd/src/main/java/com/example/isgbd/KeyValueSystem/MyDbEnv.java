package com.example.isgbd.KeyValueSystem;

import com.sleepycat.je.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MyDbEnv {

    public Environment myEnv;
    public List<Database> dbs;

    public MyDbEnv() {
    }
    public void setup(File envHome, boolean readOnly)
            throws DatabaseException {

        // Instantiate an environment and database configuration object
        EnvironmentConfig myEnvConfig = new EnvironmentConfig();
//        DatabaseConfig myDbConfig = new DatabaseConfig();
        // Configure the environment and databases for the read-only
        // state as identified by the readOnly parameter on this
        // method call.
        myEnvConfig.setReadOnly(readOnly);
        myEnvConfig.setTransactional(true);
//        myDbConfig.setReadOnly(readOnly);
//        myDbConfig.setTransactional(true);

        // If the environment is opened for write, then we want to be
        // able to create the environment and databases if
        // they do not exist.
        myEnvConfig.setAllowCreate(!readOnly);
//        myDbConfig.setAllowCreate(!readOnly);

        // Instantiate the Environment. This opens it and also possibly
        // creates it.
        myEnv = new Environment(envHome, myEnvConfig);

        dbs=new ArrayList<>();
    }
    // Getter methods
    public Environment getEnvironment() {
        return myEnv;
    }

    public Database createDatabase(String name,boolean readOnly){
        DatabaseConfig myDbConfig = new DatabaseConfig();
        myDbConfig.setReadOnly(readOnly);
        myDbConfig.setTransactional(true);
        myDbConfig.setAllowCreate(!readOnly);
        Database db=myEnv.openDatabase(null,name,myDbConfig);
        dbs.add(db);
        return db;
    }
    // Close the environment
    public void close() {
        if (myEnv != null) {
            try {
                for (Database db:dbs
                     ) {
                    db.close();
                }
                myEnv.close();
            } catch(DatabaseException dbe) {
                System.err.println("Error closing MyDbEnv: " +
                        dbe.toString());
                System.exit(-1);
            }
        }
    }
}