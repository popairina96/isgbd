package com.example.isgbd.KeyValueSystem;

public class Condition {
    public String column;
    public String eq;
    public String value;

    public Condition(String column, String eq, String value) {
        this.column = column;
        this.eq = eq;
        this.value = value;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getEq() {
        return eq;
    }

    public void setEq(String eq) {
        this.eq = eq;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
