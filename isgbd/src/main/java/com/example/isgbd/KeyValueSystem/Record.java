package com.example.isgbd.KeyValueSystem;

public class Record {
    private String key;
    private String value;

    public Record(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {

        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public boolean equal(Record r){
        if(this.key.equals(r.getKey())&&this.value.equals(r.getValue()))
            return true;
        return false;
    }

    @Override
    public String toString() {
        return "Record{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
