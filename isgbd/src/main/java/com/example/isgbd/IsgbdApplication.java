package com.example.isgbd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsgbdApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsgbdApplication.class, args);
	}
}
