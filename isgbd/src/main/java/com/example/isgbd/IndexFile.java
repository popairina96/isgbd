package com.example.isgbd;

import java.util.ArrayList;
import java.util.List;

public class IndexFile {
    public String name;
    public List<String> fields;
    public String db;
    public String table;
    public List<String> pks;
    public String isUnique;

    public IndexFile(String name, List<String> field, String db, String table, List<String> pks, String isUnique) {
        this.name = name;
        this.fields = field;
        this.db = db;
        this.table = table;
        this.pks = pks;
        this.isUnique = isUnique;
    }

    public IndexFile(String name, List<String> fields, String db, String table, String isUnique) {
        this.name = name;
        this.fields = fields;
        this.db = db;
        this.table = table;
        this.isUnique = isUnique;
    }

    public IndexFile() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<String> getPks() {
        return pks;
    }

    public void setPks(List<String> pks) {
        this.pks = pks;
    }

    public String isUnique() {
        return isUnique;
    }

    public void setUnique(String unique) {
        isUnique = unique;
    }

    @Override
    public String toString() {
        return "IndexFile{" +
                "name='" + name + '\'' +
                ", field='" + fields + '\'' +
                ", db='" + db + '\'' +
                ", table='" + table + '\'' +
                ", isUnique='" + isUnique + '\'' +
                '}';
    }
}
